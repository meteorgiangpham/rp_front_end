import { Suspense, lazy } from 'react';
import { Navigate } from 'react-router-dom';
// import { PartialRouteObject } from 'react-router';

import SidebarLayout from 'src/layouts/SidebarLayout';
import BaseLayout from 'src/layouts/BaseLayout';

import SuspenseLoader from 'src/components/SuspenseLoader';

const Loader = (Component) => (props) =>
  (
    <Suspense fallback={<SuspenseLoader />}>
      <Component {...props} />
    </Suspense>
  );

//Authentication

const Login = Loader(lazy(() => import('src/content/authentication/Login')));
const ForgotPassword = Loader(
  lazy(() => import('src/content/authentication/ForgotPassword'))
);

// Applications

const Messenger = Loader(
  lazy(() => import('src/content/applications/Messenger'))
);
const Transactions = Loader(
  lazy(() => import('src/content/applications/Transactions'))
);
const LED = Loader(lazy(() => import('src/content/applications/LED')));
const Corridor = Loader(
  lazy(() => import('src/content/applications/Corridor'))
);
const Page = Loader(lazy(() => import('src/content/applications/Page')));
const Hotspot = Loader(lazy(() => import('src/content/applications/Hotspot')));
const Preview = Loader(
  lazy(() => import('src/content/applications/Hotspot/Preview'))
);

const Audit = Loader(lazy(() => import('src/content/applications/Audit')));
const UserMgmt = Loader(
  lazy(() => import('src/content/applications/UserMgmt'))
);
// const UserProfile = Loader(
//   lazy(() => import('src/content/applications/Users/profile'))
// );
// const UserSettings = Loader(
//   lazy(() => import('src/content/applications/Users/settings'))
// );

// Components

// Status

const Status404 = Loader(
  lazy(() => import('src/content/pages/Status/Status404'))
);
const Status500 = Loader(
  lazy(() => import('src/content/pages/Status/Status500'))
);
const StatusComingSoon = Loader(
  lazy(() => import('src/content/pages/Status/ComingSoon'))
);
const StatusMaintenance = Loader(
  lazy(() => import('src/content/pages/Status/Maintenance'))
);

const routes = (isLogin) => [
  {
    path: '*',
    element: <BaseLayout />,
    children: [
      {
        path: '/',
        element: isLogin ? (
          <Navigate
            to={`${process.env.REACT_APP_BASE_NAME}/dashboards`}
            replace
          />
        ) : (
          <Navigate to={`${process.env.REACT_APP_BASE_NAME}/login`} replace />
        )
      },
      {
        path: 'overview',
        element: isLogin ? (
          <Navigate
            to={`${process.env.REACT_APP_BASE_NAME}/dashboards`}
            replace
          />
        ) : (
          <Navigate to={`${process.env.REACT_APP_BASE_NAME}/login`} replace />
        )
      },
      {
        path: 'status',
        children: [
          {
            path: '/',
            element: <Navigate to="404" replace />
          },
          {
            path: '404',
            element: <Status404 />
          },
          {
            path: '500',
            element: <Status500 />
          },
          {
            path: 'maintenance',
            element: <StatusMaintenance />
          },
          {
            path: 'coming-soon',
            element: <StatusComingSoon />
          }
        ]
      },
      {
        path: '*',
        element: <Status404 />
      }
    ]
  },
  {
    path: 'dashboards',
    element: isLogin ? (
      <SidebarLayout />
    ) : (
      <Navigate to={`${process.env.REACT_APP_BASE_NAME}/login`} replace />
    ),
    children: [
      {
        path: '/',
        element: (
          <Navigate
            to={`${process.env.REACT_APP_BASE_NAME}/dashboards/main`}
            replace
          />
        )
      },
      {
        path: 'main',
        element: <Audit />
      },
      {
        path: 'messenger',
        element: <Messenger />
      }
    ]
  },
  {
    path: 'facade',
    element: isLogin ? (
      <SidebarLayout />
    ) : (
      <Navigate to={`${process.env.REACT_APP_BASE_NAME}/login`} replace />
    ),

    children: [
      {
        path: '/',
        element: <Navigate to="/facade/screensaver" replace />
      },
      {
        path: 'screensaver',
        element: <Transactions />
      },
      {
        path: 'led',
        element: <LED />
      },
      {
        path: 'hotspot',
        element: <Hotspot />
      },
      {
        path: 'preview',
        element: <Preview />
      }
    ]
  },
  {
    path: 'corridor',
    element: isLogin ? (
      <SidebarLayout />
    ) : (
      <Navigate to={`${process.env.REACT_APP_BASE_NAME}/login`} replace />
    ),

    children: [
      {
        path: '/',
        element: <Navigate to="/corridor/screensaver" replace />
      },
      {
        path: 'screensaver',
        element: <Corridor />
      },
      {
        path: 'pages',
        element: <Page />
      }
    ]
  },
  {
    path: 'system',
    element: isLogin ? (
      <SidebarLayout />
    ) : (
      <Navigate to={`${process.env.REACT_APP_BASE_NAME}/login`} replace />
    ),

    children: [
      {
        path: '/',
        element: <Navigate to="/system/user" replace />
      },
      {
        path: 'user',
        element: <UserMgmt />
      },
      {
        path: 'audit',
        element: <Audit />
      }
    ]
  },

  {
    path: 'login',
    element: <Login />
  },
  {
    path: 'forgot-password',
    element: <ForgotPassword />
  }
];

export default routes;
