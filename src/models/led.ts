export interface Led {
  id: string;
  ledVideoUrl: string;
  thumbnailImageUrl: string;
  createdDate: string;
  updatedDate: string;
}
