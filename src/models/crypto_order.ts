export type CryptoOrderStatus = 'completed' | 'pending' | 'failed';

export interface CryptoOrder {
  id: string;
  title: string;
  mediaUrl: string;
  body: string;
  thumbnailImageUrl: string;
  ledVideoUrl: string;

  createdDate: string;
  updatedDate: string;
}
