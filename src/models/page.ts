export interface Page {
  id: string;
  title: string;
  subTitle: string;
  backgroundImageUrl: string;
  isVisible: boolean;
  contentBlocks: [];
  createdDate: string;
  description: string;
  stationId: string;
  updatedDate: string;
}
