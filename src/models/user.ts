
export type RoleAccount = 'user' | 'admin';
export interface User {
  id: string;

  firstName: string;
  lastName:string;
  createdDate:string;
  updatedDate:string;
  // status: StatusAccount;
  role: RoleAccount;
  email: string;
}
