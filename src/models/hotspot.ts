export interface Hotspot {
  id: string;
  stationId: string;
  body: string;
  coordinateX: number;
  coordinateY: number;
  highlightMediaUrl: string;
  mediaUrl: string;
  title: string;
  isVisible: boolean;
  createdDate: Date;
  updatedDate: Date;
}
