export interface Audit {
  id: string;
  user: string | null;
  userId: string;
  action: string;
  detail: string;
  fullName: string;
  createdDate: string;
  updatedDate: string;
}
