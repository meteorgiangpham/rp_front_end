export interface Corridor {
  id: string;
  title: string;
  mediaUrl: string;

  body: string;
  thumbnailImageUrl: string;
  ledVideoUrl: string;
  createdDate: string;
  updatedDate: string;
}
