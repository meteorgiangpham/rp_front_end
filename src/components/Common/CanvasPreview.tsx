import { Grid, TextField } from '@mui/material';
import { Box } from '@mui/system';
import React, { useCallback, useEffect, useRef } from 'react';
import { useForm } from 'react-hook-form';

const CanvasPreview = (props) => {
  const { x, y, image, imageBackgroundList } = props;

  const {
    register,
    setValue,
    watch,
    formState: { errors }
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange'
  });
  const canvasRef = useRef(null);

  const draw = useCallback((ctx, frameCount, x, y, image) => {
    // debugger;

    let coorX = x;
    let coorY = y;

    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

    if (image === null) {
      ctx.beginPath();
      ctx.rect(coorX, coorY, 150, 100);
      ctx.fill();
    } else {
      var img = document.getElementById('canvasImage');
      var bg = document.getElementById('canvasImageBackground');

      ctx.drawImage(bg, 0, 0, bg?.offsetWidth, bg?.offsetHeight);
      ctx.drawImage(img, coorX, coorY, img?.offsetWidth, img?.offsetHeight);
    }
  }, []);

  const coorX = useRef({});
  coorX.current = watch('coordinatesX', '');
  const coorY = useRef({});
  coorY.current = watch('coordinatesY', '');

  useEffect(() => {
    const canvas = canvasRef.current;
    const context = canvas.getContext('2d');

    let frameCount = 0;
    let animationFrameId;
    setValue('coordinatesX', x);
    setValue('coordinatesY', y);
    //Our draw came here
    const render = () => {
      frameCount++;
      if (!isNaN(x) && !isNaN(y)) {
        draw(context, frameCount, coorX.current, coorY.current, image);
        animationFrameId = window.requestAnimationFrame(render);
      }
    };
    render();

    return () => {
      window.cancelAnimationFrame(animationFrameId);
    };
  }, [draw, x, y]);

  return (
    <>
      <Grid
        container
        rowSpacing={1}
        columnSpacing={{ xs: 1, sm: 1, md: 2 }}
        sx={{ mb: 3 }}
      >
        <Grid item xs={6}>
          <TextField
            id="outlined-basic"
            label="Coordinates X"
            variant="outlined"
            InputProps={{
              inputProps: {
                min: 0,
                max: 1080,
                step: '0.01'
              }
            }}
            error={errors.coordinatesX}
            type="number"
            helperText={errors.coordinatesX && 'Warning: Max is 1080'}
            sx={{ width: '100%' }}
            {...register('coordinatesX', {
              min: 0,
              max: 1080
            })}
          />
        </Grid>
        <Grid item xs={6}>
          <TextField
            id="outlined-basic-y"
            label="Coordinates y"
            variant="outlined"
            InputProps={{
              inputProps: {
                min: 0,
                max: 1920,
                step: '0.01'
              }
            }}
            type="number"
            {...register('coordinatesY', {
              min: 0,
              max: 1920
            })}
            helperText={errors.coordinatesY && 'Warning: Max is 1920'}
            error={errors.coordinatesY}
            sx={{ width: '100%' }}
          />
        </Grid>
      </Grid>
      <canvas
        ref={canvasRef}
        {...props}
        style={{ border: '1px solid #000' }}
        width={1080}
        height={1902}
      />
      <Box height={0} overflow="hidden">
        <img
          src={image}
          alt=""
          hidden
          id="canvasImage"
          style={{ display: 'block', opacity: 0, visibility: 'hidden' }}
          width={200}
        />
        <img
          src={imageBackgroundList}
          alt=""
          hidden
          style={{ display: 'block', opacity: 0, visibility: 'hidden' }}
          id="canvasImageBackground"
          width={1080}
        />
      </Box>
    </>
  );
};

export default CanvasPreview;
