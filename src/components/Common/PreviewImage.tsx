import { useEffect, useState } from 'react';

export const PreviewImage = (props) => {
  const { selectedFile } = props;
  const [preview, setPreview] = useState<any>();
  // create a preview as a side effect, whenever selected file is changed
  useEffect(() => {
    const objectUrl = URL.createObjectURL(selectedFile);
    setPreview(objectUrl);

    // free memory when ever this component is unmounted
    return () => URL.revokeObjectURL(objectUrl);
  }, [selectedFile]);

  return (
    <div style={{ marginTop: '15px' }}>
      {selectedFile && selectedFile.type.toLowerCase().includes('mp4') ? (
        <video src={preview} width="200" height="125" controls></video>
      ) : (
        <img src={preview} alt="preview" width={300} />
      )}
      {/* {selectedFile && <img src={preview} alt="preview" width={300} />} */}
    </div>
  );
};
