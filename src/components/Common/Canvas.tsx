import React, { useRef, useEffect, useCallback } from 'react';

const Canvas = (props) => {
  const { x, y, image } = props;

  const canvasRef = useRef(null);

  const draw = useCallback((ctx, frameCount, x, y, image) => {
    // debugger;

    let coorX = (ctx.canvas.width / window.innerWidth) * x;
    let coorY = (ctx.canvas.height / window.innerHeight) * y;
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

    if (image === null) {
      ctx.beginPath();
      ctx.rect(coorX, coorY, 150, 100);
      ctx.fill();
    } else {
      var img = document.getElementById('canvasImage');
      ctx.drawImage(img, coorX, coorY, 150, 100);
    }
  }, []);

  useEffect(() => {
    const canvas = canvasRef.current;
    const context = canvas.getContext('2d');

    let frameCount = 0;
    let animationFrameId;

    //Our draw came here
    const render = () => {
      frameCount++;
      if (!isNaN(x) && !isNaN(y)) {
        draw(context, frameCount, x, y, image);
        animationFrameId = window.requestAnimationFrame(render);
      }
    };
    render();

    return () => {
      window.cancelAnimationFrame(animationFrameId);
    };
  }, [draw, x, y]);

  return (
    <>
      <canvas
        ref={canvasRef}
        {...props}
        style={{ border: '1px solid #000' }}
        width="300"
        height={500}
      />
      <img src={image} alt="" hidden id="canvasImage" />
    </>
  );
};

export default Canvas;
