import React, { useState, useRef, useMemo, useEffect } from 'react';
import JoditEditor from 'jodit-react';

const Jodit = (props) => {
  const { initialValue, limit, handleGetDataFromEditor, disable } = props;
  const editor = useRef(null);
  const [content, setContent] = useState('');

  const config = useMemo(
    () => ({
      readonly: disable ? true : false,
      limitWords: limit,
      sourceEditorCDNUrlsJS: [
        `${process.env.PUBLIC_URL}/ace.js`,
        `${process.env.PUBLIC_URL}/theme-idle_fingers.js`,
        `${process.env.PUBLIC_URL}/mode-html.js`
      ],
      beautifyHTMLCDNUrlsJS: [`${process.env.PUBLIC_URL}/beautify.min.js`]
    }),
    []
  );
  useEffect(() => {
    setContent(initialValue);
  }, [initialValue]);
  const handleChangeEditor = (newContent) => {
    setContent(newContent);
    handleGetDataFromEditor(newContent);
  };
  return (
    <JoditEditor
      ref={editor}
      value={content}
      config={config}
      tabIndex={1} // tabIndex of textarea
      onBlur={(newContent) => handleChangeEditor(newContent)} // preferred to use only this option to update the content for performance reasons
      onChange={(newContent) => {}}
    />
  );
};
export default Jodit;
