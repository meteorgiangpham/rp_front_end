import { Box, Hidden } from '@mui/material';
import { Link } from 'react-router-dom';
import { styled } from '@mui/material/styles';
import logo from '../../assets/images/revez-logo.png';
const LogoWrapper = styled(Link)(
  ({ theme }) => `
        color: ${theme.palette.text.primary};
        padding: ${theme.spacing(0, 1, 0, 0)};
        display: flex;
        text-decoration: none;
        font-weight: ${theme.typography.fontWeightBold};
`
);

const LogoTextWrapper = styled(Box)(
  ({ theme }) => `
        padding-left: ${theme.spacing(1)};
      
`
);

function Logo() {
  return (
    <LogoWrapper to={`${process.env.REACT_APP_BASE_NAME}`}>
      <Hidden smDown>
        <LogoTextWrapper>
          <Box sx={{ height: { lg: 'auto', sm: '88px' } }}></Box>
        </LogoTextWrapper>
      </Hidden>
    </LogoWrapper>
  );
}

export default Logo;
