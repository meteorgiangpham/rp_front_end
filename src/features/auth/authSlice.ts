import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  createSlice,
  PayloadAction
} from '@reduxjs/toolkit';
import { LoginForm } from '../../models';
import { fetchApi } from './authAPI';

export interface AuthState {
  isLoggedIn?: boolean;
  loggin?: boolean;
}

export const loginAsync = createAsyncThunk(
  'auth/fetchApi',
  async (payload: LoginForm) => {
    const response = await fetchApi(payload);

    // if(response.success)
    return response;
    // else return {success: false, data: response.data}
    // if (typeof response == "string") {
    //   return response;
    // } else {
    //   return "error";
    // }
  }
);

const initialState: AuthState = {
  isLoggedIn: false,
  loggin: false
};
const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    login(state, action: PayloadAction<LoginForm>) {
      state.loggin = true;
    },
    loginSuccess(state, action: PayloadAction<string>) {
      state.loggin = false;
      state.isLoggedIn = true;
    }
  },

  extraReducers: (builder: ActionReducerMapBuilder<AuthState>) => {
    builder
      .addCase(loginAsync.pending, (state) => {
        state.loggin = true;
      })
      .addCase(loginAsync.fulfilled, (state, action) => {
        state.loggin = false;
        if (action.payload.success) {
          state.isLoggedIn = action.payload.success;
        }
      })
      .addCase(loginAsync.rejected, (state) => {
        state.loggin = false;
      });
  }
});

//Actions
export const { loginSuccess, login } = authSlice.actions;
//Selectors
export const selectIsLoginIn = (state: any) => state.auth.isLoggedIn;
export const selectIsLogin = (state: any) => state.auth.loggin;

//Reducers

const authReducer = authSlice.reducer;
export default authReducer;
