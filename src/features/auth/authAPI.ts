import loginApi from 'src/api/loginApi';
import { LoginForm } from 'src/models';

export function fetchApi(payload: LoginForm) {
  const success = loginApi
    .login(payload)
    .then((res) => res.data)
    .catch((err) => err);

  return success;
}
