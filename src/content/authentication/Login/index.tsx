import {
  Box,
  Button,
  Card,
  Container,
  Snackbar,
  TextField,
  Typography
} from '@mui/material';
import { useContext, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import loginApi from 'src/api/loginApi';
import { AuthContext } from 'src/App';
import bg from '../../../assets/images/697601.jpg';

function Login() {
  const { handleLoginIn } = useContext(AuthContext);
  const { register, handleSubmit } = useForm();
  const nav = useNavigate();
  const [open, setOpen] = useState<boolean>(false);
  const [errMsg, setErrMsg] = useState<string>('');
  const handleClose = () => {
    setOpen(false);
  };
  const onSubmit = (data) => {
    const { username, password } = data;
    loginApi.login({ userName: username, password }).then((res) => {
      if (res.data.success) {
        localStorage.setItem('access_token', res.data.data.token);

        handleLoginIn();
        nav(`${process.env.REACT_APP_BASE_NAME}/dashboards/main`);
      } else {
        setErrMsg(res.data.data);
        setOpen(true);
      }
    });

    //
  };

  return (
    <Box
      sx={{
        width: '100vw',
        height: '100vh',
        background: `url(${bg})`,
        backgroundSize: 'cover'
      }}
    >
      <Container maxWidth="lg" sx={{ height: '100vh', pt: 15 }}>
        <Card sx={{ p: { md: 10, xs: 3 } }}>
          <Typography variant="h1" component="h1" align="center" sx={{ mb: 2 }}>
            Login
          </Typography>
          <Typography variant="h3" component="h3" align="center">
            Content Management System
          </Typography>
          <Box
            sx={{
              display: 'flex',
              alignItems: 'center',
              flexDirection: 'column'
            }}
          >
            <Box
              component="form"
              sx={{
                '& .MuiTextField-root': {
                  width: { md: '70ch', xs: '100%' },
                  mb: 4
                },
                '& button': {
                  width: { md: '61ch', xs: '100%' },
                  height: '53px'
                },

                width: { md: '70ch', xs: '100%' },
                mt: 10
              }}
              noValidate
              autoComplete="off"
              onSubmit={handleSubmit(onSubmit)}
            >
              <TextField
                id="outlined-username-input"
                label="Username"
                type="text"
                {...register('username')}
              />
              <TextField
                id="outlined-password-input"
                label="Password"
                type="password"
                autoComplete="current-password"
                {...register('password')}
              />

              <Button size="large" variant="contained" type="submit">
                LOGIN
              </Button>
            </Box>
          </Box>
        </Card>
        <Snackbar
          autoHideDuration={3000}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={open}
          onClose={handleClose}
          message={errMsg}
          key={'top right'}
        />
      </Container>
    </Box>
  );
}
export default Login;
