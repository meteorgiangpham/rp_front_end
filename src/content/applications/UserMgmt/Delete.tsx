import PropTypes from 'prop-types';
import { useContext, useState } from 'react';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import DeleteTwoToneIcon from '@mui/icons-material/DeleteTwoTone';
import {
  Box,
  Button,
  DialogContent,
  Divider,
  Snackbar,
  Typography,
  CircularProgress
} from '@mui/material';
import userApi from 'src/api/userApi';
import { AuthContext } from 'src/App';
function SimpleDialog(props) {
  const { onClose, open, id, changeToastMessage, handleOpenToast } = props;

  const { updateSuccess } = useContext(AuthContext);

  const [loading, setLoading] = useState<boolean>(false);

  const deleteUser = (id: string) => {
    userApi.deleteUser(id).then((res) => {
      if (res.data.success) {
        onClose();
        handleOpenToast();
        changeToastMessage(res.data.message);
        updateSuccess();
        setLoading(false);
      } else {
        changeToastMessage('Error');
        setLoading(false);
        handleOpenToast();
      }
    });
  };
  return (
    <Dialog maxWidth="sm" fullWidth={true} onClose={onClose} open={open}>
      <DialogTitle>
        {' '}
        <Typography
          variant="h3"
          sx={{ fontWeight: 'bold', textTransform: 'uppercase' }}
        >
          Delete User
        </Typography>
      </DialogTitle>
      <DialogContent>
        <Box sx={{ mb: 3 }}>
          <Typography variant="h5">Are you sure about this action?</Typography>
        </Box>
        <Divider sx={{ mb: 1 }} />
        <Box sx={{ display: 'flex', justifyContent: 'flex-end' }}>
          <Box>
            {' '}
            <Button
              color="warning"
              variant="contained"
              sx={{ mr: 2 }}
              onClick={() => {
                deleteUser(id);
              }}
            >
              {loading ? <CircularProgress color="info" /> : 'Delete'}
            </Button>
            <Button color="primary" variant="contained" onClick={onClose}>
              Close
            </Button>
          </Box>
        </Box>
      </DialogContent>
    </Dialog>
  );
}

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  id: PropTypes.string.isRequired,

  handleOpenToast: PropTypes.func.isRequired,
  changeToastMessage: PropTypes.func.isRequired
};

function Delete(props) {
  const { id } = props;
  const [open, setOpen] = useState(false);
  const [openToast, setOpenToast] = useState(false);
  const [msg, setMsg] = useState<string>('');
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleCloseToast = () => {
    setOpenToast(false);
  };

  const handleOpenToast = () => {
    setOpenToast(true);
  };

  const changeToastMessage = (msg: string) => {
    setMsg(msg);
  };

  return (
    <>
      <DeleteTwoToneIcon fontSize="small" onClick={handleClickOpen} />

      <SimpleDialog
        open={open}
        onClose={handleClose}
        id={id}
        changeToastMessage={changeToastMessage}
        handleOpenToast={handleOpenToast}
      />
      <Snackbar
        autoHideDuration={3000}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={openToast}
        onClose={handleCloseToast}
        message={msg}
        key={'top right'}
      />
    </>
  );
}

export default Delete;
