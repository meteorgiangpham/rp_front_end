import { Card, LinearProgress } from '@mui/material';
import { useContext, useEffect, useState } from 'react';
import userApi from 'src/api/userApi';
import { AuthContext } from 'src/App';
import { User } from 'src/models';
import RecentOrdersTable from './RecentOrdersTable';

function RecentOrders() {
  const [loading, setLoading] = useState<boolean>(false);
  const { updated } = useContext(AuthContext);
  useEffect(() => {
    setLoading(true);
    userApi.getList().then((res) => {
      if (res.data.success) {
        setLoading(false);
        setCryptoOrders(res.data.data);
      }
    });
  }, [updated]);
  const [cryptoOrders, setCryptoOrders] = useState<User[]>([]);
  return (
    <Card>
      <RecentOrdersTable cryptoOrders={cryptoOrders} />
      {loading && <LinearProgress />}
    </Card>
  );
}

export default RecentOrders;
