import PropTypes from 'prop-types';
import React, { useContext, useEffect, useRef, useState } from 'react';
import Button from '@mui/material/Button';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import AddTwoToneIcon from '@mui/icons-material/AddTwoTone';
import {
  Box,
  DialogContent,
  MenuItem,
  Select,
  Snackbar,
  TextField,
  Typography
} from '@mui/material';
import { useForm } from 'react-hook-form';
import userApi from 'src/api/userApi';
import { AuthContext } from 'src/App';

function SimpleDialog(props) {
  const { updateSuccess } = useContext(AuthContext);
  const { onClose, open, handleOpenToast, changeToastMessage } = props;

  // React-hook-form
  const {
    register,
    handleSubmit,
    setValue,
    watch,
    formState: { errors },
    trigger,
    reset
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange'
  });
  const onSubmit = (data) => {
    const { firstname, lastname, email, password, role } = data;
    userApi
      .addUser({
        firstName: firstname,
        lastName: lastname,
        email,
        password,
        role
      })
      .then((res) => {
        if (res.data.success) {
          reset({
            data: 'test'
          });
          onClose();
          handleOpenToast();
          changeToastMessage(res.data.message);
          updateSuccess();
        } else {
          handleOpenToast();
          changeToastMessage(res.data.message);
        }
      });
  };
  const [role, setRole] = useState<string>('Admin');

  useEffect(() => {
    register('role');
  });

  const handleChangeRole = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRole(event.target.value);
    setValue('role', event.target.value);
    trigger('role');
  };
  const password = useRef({});
  password.current = watch('password', '');
  return (
    <Dialog maxWidth="md" fullWidth={true} onClose={onClose} open={open}>
      <DialogTitle>
        {' '}
        <Typography
          variant="h3"
          sx={{ fontWeight: 'bold', textTransform: 'uppercase' }}
        >
          Add User
        </Typography>
      </DialogTitle>
      <DialogContent sx={{ width: '100%' }}>
        <Box component="form" onSubmit={handleSubmit(onSubmit)}>
          <Box sx={{ mt: 3 }}>
            <TextField
              required
              id="outlined-basic"
              label="Firstname"
              variant="outlined"
              error={errors.title}
              {...register('title', {
                required: 'Title is require',
                maxLength: {
                  value: 70,
                  message: 'Max length is 70 characters'
                }
              })}
              sx={{ width: '100%' }}
            />
          </Box>
          {errors.title && (
            <Typography color="error">{errors.title.message}</Typography>
          )}
          <Box sx={{ mt: 3 }}>
            <TextField
              required
              id="outlined-basic"
              label="Lastname"
              variant="outlined"
              {...register('lastname', { required: true, maxLength: 70 })}
              sx={{ width: '100%' }}
            />
          </Box>
          <Box sx={{ mt: 3 }}>
            <TextField
              required
              type="email"
              id="outlined-basic"
              label="Email"
              variant="outlined"
              {...register('email', { required: true, maxLength: 70 })}
              sx={{ width: '100%' }}
            />
          </Box>
          <Box sx={{ mt: 3 }}>
            <TextField
              required
              type="password"
              id="outlined-basic"
              label="Password"
              variant="outlined"
              {...register('password', { required: true, maxLength: 70 })}
              sx={{ width: '100%' }}
            />
          </Box>
          <Box sx={{ mt: 3 }}>
            <TextField
              required
              type="password"
              id="outlined-basic"
              label="Confirm Password"
              variant="outlined"
              {...register('confirmPassword', {
                required: true,
                maxLength: 70,
                validate: (value) =>
                  value === password.current || 'The passwords do not match'
              })}
              sx={{ width: '100%' }}
            />
            {errors.confirmPassword && <p>{errors.confirmPassword.message}</p>}
          </Box>

          <Box sx={{ mt: 3 }}>
            <Typography>Select role</Typography>
            <Select
              value={role}
              onChange={handleChangeRole}
              displayEmpty
              inputProps={{ 'aria-label': 'Without label' }}
            >
              <MenuItem value="Admin">Admin</MenuItem>
              <MenuItem value="User">User</MenuItem>
            </Select>
          </Box>

          <Box sx={{ mt: 3 }}>
            <Button
              color="secondary"
              type="submit"
              variant="contained"
              sx={{ width: '100%' }}
            >
              Add user
            </Button>
          </Box>
        </Box>
      </DialogContent>
    </Dialog>
  );
}

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,

  handleOpenToast: PropTypes.func.isRequired,
  changeToastMessage: PropTypes.func.isRequired
};

function Add() {
  const [open, setOpen] = useState(false);
  const [openToast, setOpenToast] = useState(false);
  const [msg, setMsg] = useState<string>('');
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleCloseToast = () => {
    setOpenToast(false);
  };

  const handleOpenToast = () => {
    setOpenToast(false);
  };

  const changeToastMessage = (msg: string) => {
    setMsg(msg);
  };

  return (
    <>
      <Button
        onClick={handleClickOpen}
        sx={{ mt: { xs: 2, md: 0 } }}
        variant="contained"
        startIcon={<AddTwoToneIcon fontSize="small" />}
      >
        Create
      </Button>
      <SimpleDialog
        open={open}
        onClose={handleClose}
        changeToastMessage={changeToastMessage}
        handleOpenToast={handleOpenToast}
      />
      <Snackbar
        autoHideDuration={3000}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={openToast}
        onClose={handleCloseToast}
        message={msg}
        key={'top right'}
      />
    </>
  );
}

export default Add;
