import EditTwoToneIcon from '@mui/icons-material/EditTwoTone';
import {
  Box,
  Button,
  CircularProgress,
  DialogContent,
  MenuItem,
  Select,
  Snackbar,
  TextField,
  Typography
} from '@mui/material';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import PropTypes from 'prop-types';
import { useContext, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import userApi from 'src/api/userApi';
import { AuthContext } from 'src/App';
function SimpleDialog(props) {
  const { updateSuccess } = useContext(AuthContext);
  const { onClose, open, idEdit, changeToastMessage, handleOpenToast } = props;
  const [loading, setLoading] = useState<boolean>(false);
  // React-hook-form

  const {
    register,
    handleSubmit,
    setValue,
    trigger,
    formState: { errors }
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange'
  });
  const onSubmit = (data) => {
    setLoading(true);
    const { lastname, firstname, role, email, id } = data;
    userApi
      .editUser({ id, lastName: lastname, firstName: firstname, role, email })
      .then((res) => {
        if (res.data.success) {
          onClose();
          handleOpenToast();
          changeToastMessage(res.data.message);
          updateSuccess();
          setLoading(false);
        } else {
          changeToastMessage(res.data.message);
          setLoading(false);
          handleOpenToast();
        }
      });
  };
  const [role, setRole] = useState<string>('Admin');

  useEffect(() => {
    register('id');
    register('userId');
    setValue('role', 'Admin');
  }, []);

  const handleChangeRole = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRole(event.target.value);
    setValue('role', event.target.value);
    trigger('role');
  };

  useEffect(() => {
    if (open) {
      userApi.getUserById(idEdit).then((res) => {
        if (res.data.success) {
          let data = res.data.data;
          setValue('id', data.id);
          trigger('id');
          setValue('email', data.email);
          trigger('email');
          setValue('firstname', data.firstName);
          trigger('firstname');
          setValue('lastname', data.lastName);
          trigger('lastname');
          setRole(data.role);

          setValue('role', data.role);
          trigger('role');
        }
      });
    }
  }, [open]);

  return (
    <Dialog maxWidth="md" fullWidth={true} onClose={onClose} open={open}>
      <DialogTitle>
        {' '}
        <Typography
          variant="h3"
          sx={{ fontWeight: 'bold', textTransform: 'uppercase' }}
        >
          Edit User
        </Typography>
      </DialogTitle>
      <DialogContent sx={{ width: '100%' }}>
        <Box component="form" onSubmit={handleSubmit(onSubmit)}>
          <Box sx={{ mt: 3 }}>
            <TextField
              required
              id="outlined-basic"
              label="Firstname"
              defaultValue={'firstname'}
              variant="outlined"
              {...register('firstname', {
                required: 'Title is require',
                maxLength: {
                  value: 70,
                  message: 'Max length is 70 characters'
                }
              })}
              sx={{ width: '100%' }}
            />
          </Box>
          {errors.title && (
            <Typography color="error">{errors.firstname.message}</Typography>
          )}
          <Box sx={{ mt: 3 }}>
            <TextField
              required
              id="outlined-basic"
              label="Lastname"
              defaultValue={'lastname'}
              variant="outlined"
              {...register('lastname', { required: true, maxLength: 70 })}
              sx={{ width: '100%' }}
            />
          </Box>
          <Box sx={{ mt: 3 }}>
            <TextField
              required
              type="email"
              id="outlined-basic"
              label="Email"
              defaultValue={'email'}
              disabled={true}
              variant="outlined"
              {...register('email', { required: true, maxLength: 70 })}
              sx={{ width: '100%' }}
            />
          </Box>

          <Box sx={{ mt: 3 }}>
            <Typography>Select role</Typography>
            <Select
              value={role}
              onChange={handleChangeRole}
              displayEmpty
              inputProps={{ 'aria-label': 'Without label' }}
            >
              <MenuItem value="Admin">Admin</MenuItem>
              <MenuItem value="User">User</MenuItem>
            </Select>
          </Box>

          <Box sx={{ mt: 3 }}>
            <Button
              color="secondary"
              type="submit"
              variant="contained"
              sx={{ width: '100%' }}
            >
              {loading ? <CircularProgress color="info" /> : 'Submit'}
            </Button>
          </Box>
        </Box>
      </DialogContent>
    </Dialog>
  );
}

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  idEdit: PropTypes.string.isRequired,
  handleOpenToast: PropTypes.func.isRequired,
  changeToastMessage: PropTypes.func.isRequired
};
interface PropsId {
  id: string;
}
function Edit({ id }: PropsId) {
  const [open, setOpen] = useState(false);
  const [openToast, setOpenToast] = useState(false);
  const [msg, setMsg] = useState<string>('');
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleCloseToast = () => {
    setOpenToast(false);
  };

  const handleOpenToast = () => {
    setOpenToast(true);
  };

  const changeToastMessage = (msg: string) => {
    setMsg(msg);
  };
  return (
    <>
      <EditTwoToneIcon fontSize="small" onClick={handleClickOpen} />

      <SimpleDialog
        open={open}
        onClose={handleClose}
        idEdit={id}
        changeToastMessage={changeToastMessage}
        handleOpenToast={handleOpenToast}
      />
      <Snackbar
        autoHideDuration={3000}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={openToast}
        onClose={handleCloseToast}
        message={msg}
        key={'top right'}
      />
    </>
  );
}
export default Edit;
