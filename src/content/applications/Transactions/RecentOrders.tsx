import { Card, LinearProgress } from '@mui/material';
import { useContext, useEffect, useState } from 'react';
import loginApi from 'src/api/loginApi';
import station from 'src/api/station';
import { AuthContext } from 'src/App';
import { CryptoOrder } from 'src/models/crypto_order';
import RecentOrdersTable from './RecentOrdersTable';

function RecentOrders() {
  const [loading, setLoading] = useState<boolean>(false);
  const { updated } = useContext(AuthContext);
  useEffect(() => {
    setLoading(true);
    station.getSation().then((res) => {
      if (res.data.success) {
        loginApi.getFacadeA(res.data.data.id).then((res) => {
          if (res.data.success) {
            setLoading(false);
            setCryptoOrders(res.data.data);
          }
        });
      }
    });
  }, [updated]);
  const [cryptoOrders, setCryptoOrders] = useState<CryptoOrder[]>([]);

  return (
    <Card>
      <RecentOrdersTable cryptoOrders={cryptoOrders} />
      {loading && <LinearProgress />}
    </Card>
  );
}

export default RecentOrders;
