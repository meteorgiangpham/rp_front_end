import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import CloseIcon from '@mui/icons-material/Close';
import EditTwoToneIcon from '@mui/icons-material/EditTwoTone';
import {
  Box,
  Button,
  CircularProgress,
  DialogContent,
  Snackbar,
  TextField,
  Typography
} from '@mui/material';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import PropTypes from 'prop-types';
import { useContext, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import loginApi from 'src/api/loginApi';
import station from 'src/api/station';
import { AuthContext } from 'src/App';
import { PreviewImage } from 'src/components/Common/PreviewImage';

function SimpleDialog(props) {
  const { updateSuccess } = useContext(AuthContext);
  const { onClose, open, idEdit, handleOpenToast, changeToastMessage } = props;
  const [loading, setLoading] = useState<boolean>(false);
  const [listDelete, setListDelete] = useState<any>([]);
  const [allMedia, setAllMedia] = useState({
    ledVideoUrl: '',
    thumbnailImageUrl: ''
  });
  // React-hook-form

  const {
    register,
    getValues,
    handleSubmit,
    setValue,
    trigger,
    watch,
    formState: { errors }
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange'
  });
  const onSubmit = async (data) => {
    setLoading(true);
    const { title, body, video } = data;

    const formData = new FormData();
    formData.append('Id', idEdit);
    formData.append('Title', title);
    formData.append('Body', body);

    if (listDelete.length > 0) {
      listDelete.forEach((item, index) => {
        formData.append(`ImageDeletes[${index}].Id`, item.id);
        formData.append(`ImageDeletes[${index}].FileName`, item.fileName);
      });
    }

    if (
      video !== undefined &&
      video[0] &&
      (video[0].type.toLowerCase() === 'image/jpg' ||
        video[0].type.toLowerCase() === 'image/jpeg' ||
        video[0].type.toLowerCase() === 'image/png')
    ) {
      formData.append('LedVideo', video[0]);
    }

    try {
      await loginApi.editFacadeAById(formData).then((res) => {
        setLoading(false);

        if (res.data.success) {
          onClose();
          handleOpenToast();
          changeToastMessage(res.data.message);
          setValue('video', undefined);
          updateSuccess();
        } else {
          changeToastMessage(res.data.message);
          handleOpenToast();
        }
      });
    } catch (error) {
      setLoading(false);
    }
  };
  useEffect(() => {
    if (open) {
      station.getSation().then((res) => {
        if (res.data.success) {
          loginApi.getFacadeAById(res.data.data.id, idEdit).then((res) => {
            if (res.data.success) {
              const data = res.data.data;
              setValue('title', data.title ? data.title : '');
              setValue('body', data.body ? data.body : '');

              setAllMedia({
                ledVideoUrl: data.ledVideoUrl ? data.ledVideoUrl : '',
                thumbnailImageUrl: data.thumbnailImageUrl
                  ? data.thumbnailImageUrl
                  : ''
              });
            }
          });
        }
      });
    }
  }, [idEdit, setValue, open]);
  useEffect(() => {
    register('body');
  });
  const watchVideo = watch(['video', 'thumbnail']);

  const removeFile = (name: string) => {
    setValue(name, undefined);
  };

  const removeLocalMedia = (name, slot) => {
    let fileName = name.slice(name.lastIndexOf('/') + 1);
    let arr = listDelete;
    arr.push({ id: idEdit, fileName: fileName });
    setListDelete(arr);
    setAllMedia({ ...allMedia, [slot]: '' });
  };
  const handleListFile = (file: File) => {
    if (file) {
      if (
        file.type.toLowerCase() === 'image/jpg' ||
        file.type.toLowerCase() === 'image/jpeg' ||
        file.type.toLowerCase() === 'image/png'
      ) {
        if (file.size <= 5012 * 1024)
          return <PreviewImage selectedFile={file} />;
        else return <p style={{ color: 'red' }}>This file is too large</p>;
      } else return <p style={{ color: 'red' }}>Incorrect format</p>;
    }
  };

  return (
    <Dialog maxWidth="md" fullWidth={true} onClose={onClose} open={open}>
      <DialogTitle>
        {' '}
        <Typography
          variant="h3"
          sx={{ fontWeight: 'bold', textTransform: 'uppercase' }}
        >
          Edit Touchscreen Screensaver
        </Typography>
      </DialogTitle>
      <DialogContent sx={{ width: '100%' }}>
        <Box component="form" onSubmit={handleSubmit(onSubmit)}>
          <Box sx={{ mt: 3 }}>
            <TextField
              required
              id="outlined-basic"
              label="Title (max. 70 characters)"
              variant="outlined"
              defaultValue="0"
              error={errors.title}
              {...register('title', {
                required: 'Title is require',
                maxLength: {
                  value: 70,
                  message: 'Max length is 70 characters'
                }
              })}
              sx={{ width: '100%' }}
            />
          </Box>
          {errors.title && (
            <Typography color="error">{errors.title.message}</Typography>
          )}
          <Box sx={{ mt: 3 }}>
            <Typography>Body text *Optional* (max. 300 characters)</Typography>
            <CKEditor
              editor={ClassicEditor}
              data={getValues('body')}
              onChange={(event, editor) => {
                setValue('body', editor.getData() ? editor.getData() : '');
                trigger('body');
              }}
            />
          </Box>

          <Box sx={{ mt: 3 }}>
            <Typography>
              Touchscreen Screensaver (9:16, 1080 x 1920 px, JPG/PNG max. 5MB)
            </Typography>
            <Button variant="contained" component="label" color="secondary">
              Upload File
              <input
                type="file"
                hidden
                {...register('video')}
                accept=".jpg, .png"
              />
            </Button>

            {watchVideo[0] && watchVideo[0][0] && (
              <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
                {handleListFile(watchVideo[0][0])}{' '}
                <CloseIcon
                  color="error"
                  style={{ cursor: 'pointer' }}
                  onClick={() => {
                    removeFile('video');
                  }}
                />
              </Box>
            )}
            {(!watchVideo[0] || !watchVideo[0][0]) && allMedia.ledVideoUrl && (
              <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
                <Typography sx={{ mt: 3 }}>
                  {allMedia.ledVideoUrl.includes('mp4') ? (
                    <video
                      src={allMedia.ledVideoUrl}
                      width="200"
                      height="125"
                      controls
                    ></video>
                  ) : (
                    <img
                      src={allMedia.ledVideoUrl}
                      alt="led"
                      width="200"
                      height="auto"
                    />
                  )}
                </Typography>
                <CloseIcon
                  color="error"
                  style={{ cursor: 'pointer' }}
                  onClick={() => {
                    removeLocalMedia(allMedia.ledVideoUrl, 'ledVideoUrl');
                  }}
                />
              </Box>
            )}
          </Box>

          <Box sx={{ mt: 3 }}>
            <Button
              color="secondary"
              type="submit"
              variant="contained"
              sx={{ width: '100%' }}
            >
              {loading ? <CircularProgress color="info" /> : 'Submit'}
            </Button>
          </Box>
        </Box>
      </DialogContent>
    </Dialog>
  );
}

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  idEdit: PropTypes.string.isRequired,
  handleOpenToast: PropTypes.func.isRequired,
  changeToastMessage: PropTypes.func.isRequired
};

interface PropsId {
  id: string;
}
function Edit({ id }: PropsId) {
  const [open, setOpen] = useState(false);
  const [openToast, setOpenToast] = useState(false);
  const [msg, setMsg] = useState<string>('');
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleCloseToast = () => {
    setOpenToast(false);
  };

  const handleOpenToast = () => {
    setOpenToast(true);
  };

  const changeToastMessage = (msg: string) => {
    setMsg(msg);
  };
  return (
    <>
      <EditTwoToneIcon fontSize="small" onClick={handleClickOpen} />

      <SimpleDialog
        open={open}
        onClose={handleClose}
        idEdit={id}
        changeToastMessage={changeToastMessage}
        handleOpenToast={handleOpenToast}
      />
      <Snackbar
        autoHideDuration={3000}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={openToast}
        onClose={handleCloseToast}
        message={msg}
        key={'top right'}
      />
    </>
  );
}

export default Edit;
