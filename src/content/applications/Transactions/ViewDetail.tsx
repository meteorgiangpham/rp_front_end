import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import CloseIcon from '@mui/icons-material/Close';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import {
  Box,
  DialogContent,
  Snackbar,
  TextField,
  Typography
} from '@mui/material';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import PropTypes from 'prop-types';
import { useContext, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import loginApi from 'src/api/loginApi';
import station from 'src/api/station';
import { AuthContext } from 'src/App';

function SimpleDialog(props) {
  const { updateSuccess } = useContext(AuthContext);
  const {
    onClose,
    open,
    idEdit,
    disable,
    handleOpenToast,
    changeToastMessage
  } = props;

  const [allMedia, setAllMedia] = useState({
    ledVideoUrl: '',
    thumbnailImageUrl: ''
  });

  const { register, handleSubmit, setValue, getValues, trigger, watch } =
    useForm({
      mode: 'onChange'
    });
  const onSubmit = async (data) => {
    const { title, body, video } = data;
    const formData = new FormData();
    formData.append('Id', idEdit);
    formData.append('Title', title);
    formData.append('Body', body);

    if (
      video[0] &&
      (video[0].type.toLowerCase() === 'image/jpg' ||
        video[0].type.toLowerCase() === 'image/jpeg' ||
        video[0].type.toLowerCase() === 'image/png')
    ) {
      formData.append('LedVideo', video[0]);
    }

    try {
      await loginApi.editFacadeAById(formData).then((res) => {
        if (res.data.success) {
          onClose();
          handleOpenToast();
          changeToastMessage(res.data.message);
          updateSuccess();
        } else {
          changeToastMessage('Error');

          handleOpenToast();
        }
      });
    } catch (error) {}
  };
  useEffect(() => {
    station.getSation().then((res) => {
      if (res.data.success) {
        loginApi.getFacadeAById(res.data.data.id, idEdit).then((res) => {
          if (res.data.success) {
            const data = res.data.data;
            setValue('title', data.title);
            setValue('body', data.body);

            setAllMedia({
              ledVideoUrl: data.ledVideoUrl,
              thumbnailImageUrl: data.thumbnailImageUrl
            });
          }
        });
      }
    });
  }, [idEdit, setValue]);
  useEffect(() => {
    register('body');
  });
  const watchVideo = watch(['video', 'thumbnail']);

  const removeFile = (name: string) => {
    setValue(name, undefined);
  };

  const handleListFile = (file: File) => {
    if (file) {
      if (
        file.type.toLowerCase() === 'image/jpg' ||
        file.type.toLowerCase() === 'image/jpeg' ||
        file.type.toLowerCase() === 'image/png'
      ) {
        if (file.size <= 5012 * 1024) return <p>{file.name}</p>;
        else return <p style={{ color: 'red' }}>This file is too large</p>;
      } else return <p style={{ color: 'red' }}>Incorrect format</p>;
    }
  };

  return (
    <Dialog maxWidth="md" fullWidth={true} onClose={onClose} open={open}>
      <DialogTitle>
        {' '}
        <Typography
          variant="h3"
          sx={{ fontWeight: 'bold', textTransform: 'uppercase' }}
        >
          View Detail Touchscreen Screensaver
        </Typography>
      </DialogTitle>
      <DialogContent sx={{ width: '100%' }}>
        <Box component="form" onSubmit={handleSubmit(onSubmit)}>
          <Box sx={{ mt: 3 }}>
            <TextField
              required
              disabled={disable}
              id="outlined-basic"
              label="Title (max. 70 characters)"
              variant="outlined"
              {...register('title', { required: true, maxLength: 70 })}
              sx={{ width: '100%' }}
            />
          </Box>
          <Box sx={{ mt: 3 }}>
            <Typography>Body text *Optional* (max. 300 characters)</Typography>
            <CKEditor
              editor={ClassicEditor}
              data={getValues('body')}
              onChange={(event, editor) => {
                setValue('body', editor.getData());
                trigger('body');
              }}
              disabled={disable}
            />
          </Box>

          <Box sx={{ mt: 3 }}>
            <Typography>
              Touchscreen Screensaver (9:16, 1080 x 1920 px, JPG/PNG max. 5MB)
            </Typography>

            {watchVideo[0] && watchVideo[0][0] && (
              <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
                {handleListFile(watchVideo[0][0])}{' '}
                <CloseIcon
                  onClick={() => {
                    removeFile('video');
                  }}
                />
              </Box>
            )}
            {(!watchVideo[0] || !watchVideo[0][0]) && allMedia.ledVideoUrl && (
              <Typography sx={{ mt: 3 }}>
                {allMedia.ledVideoUrl.includes('mp4') ? (
                  <video
                    src={allMedia.ledVideoUrl}
                    width="200"
                    height="125"
                    controls
                  ></video>
                ) : (
                  <img
                    src={allMedia.ledVideoUrl}
                    alt="led"
                    width="200"
                    height="auto"
                  />
                )}
              </Typography>
            )}
          </Box>
        </Box>
      </DialogContent>
    </Dialog>
  );
}

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  idEdit: PropTypes.string.isRequired,
  handleOpenToast: PropTypes.func.isRequired,
  disable: PropTypes.bool.isRequired,
  changeToastMessage: PropTypes.func.isRequired
};

interface PropsId {
  id: string;
}
function Edit({ id }: PropsId) {
  const [open, setOpen] = useState(false);
  const [openToast, setOpenToast] = useState(false);
  const [msg, setMsg] = useState<string>('');
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleCloseToast = () => {
    setOpenToast(false);
  };

  const handleOpenToast = () => {
    setOpenToast(true);
  };

  const changeToastMessage = (msg: string) => {
    setMsg(msg);
  };
  return (
    <>
      <RemoveRedEyeIcon fontSize="small" onClick={handleClickOpen} />

      <SimpleDialog
        open={open}
        onClose={handleClose}
        disable={true}
        idEdit={id}
        changeToastMessage={changeToastMessage}
        handleOpenToast={handleOpenToast}
      />
      <Snackbar
        autoHideDuration={3000}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={openToast}
        onClose={handleCloseToast}
        message={msg}
        key={'top right'}
      />
    </>
  );
}

export default Edit;
