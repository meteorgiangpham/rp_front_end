import CloseIcon from '@mui/icons-material/Close';
import EditTwoToneIcon from '@mui/icons-material/EditTwoTone';
import {
  Box,
  Button,
  CircularProgress,
  DialogContent,
  Snackbar,
  TextField,
  Typography
} from '@mui/material';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import PropTypes from 'prop-types';
import { useContext, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import corridorBApi from 'src/api/corridorBApi';
import station from 'src/api/station';
import { AuthContext } from 'src/App';
import { PreviewImage } from 'src/components/Common/PreviewImage';
import Jodit from 'src/components/TinyEditor/JoditEditor';

function SimpleDialog(props) {
  const { updateSuccess } = useContext(AuthContext);
  const { onClose, open, idEdit, handleOpenToast, changeToastMessage } = props;
  const [loading, setLoading] = useState<boolean>(false);
  const [listDelete, setListDelete] = useState<any>([]);
  const [allMedia, setAllMedia] = useState({
    ledVideoUrl: '',
    mediaUrl: '',
    thumbnailImageUrl: ''
  });
  const {
    register,
    getValues,
    handleSubmit,
    setValue,
    trigger,
    watch,
    formState: { errors }
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange'
  });
  const onSubmit = async (data) => {
    const { title, body, media } = data;
    setLoading(true);
    const formData = new FormData();
    formData.append('Id', idEdit);
    formData.append('Title', title);
    formData.append('Text', body);

    if (media !== undefined && media[0] !== undefined) {
      formData.append('Media', media[0]);
    }
    if (listDelete.length > 0) {
      listDelete.forEach((item, index) => {
        formData.append(`ImageDeletes[${index}].Id`, item.id);
        formData.append(`ImageDeletes[${index}].FileName`, item.fileName);
      });
    }
    try {
      await corridorBApi.editCorridorB(formData).then((res) => {
        setLoading(false);

        if (res.data.success) {
          onClose();
          handleOpenToast();
          changeToastMessage(res.data.message);
          setValue('media', undefined);
          updateSuccess();
        } else {
          changeToastMessage(res.data.message);
          handleOpenToast();
        }
      });
    } catch (error) {
      setLoading(false);
    }
  };

  const handleUploadFile = (file: File) => {
    if (
      file.type.toLowerCase() === 'image/jpg' ||
      file.type.toLowerCase() === 'image/jpeg' ||
      file.type.toLowerCase() === 'image/png'
    ) {
      if (file.size <= 5012 * 1024) {
        return <PreviewImage selectedFile={file} />;
      } else return <p style={{ color: 'red' }}>This file is too large</p>;
    } else return <p style={{ color: 'red' }}>Incorrect format</p>;
  };

  useEffect(() => {
    station.getSation2().then((res) => {
      if (res.data.success) {
        corridorBApi.getCorridorBById(res.data.data.id, idEdit).then((res) => {
          if (res.data.success) {
            const data = res.data.data;

            setValue('title', data.title);
            setValue('body', data.body ? data.body : '');

            setAllMedia({
              ledVideoUrl: data.ledVideoUrl,
              mediaUrl: data.mediaUrl,
              thumbnailImageUrl: data.thumbnailImageUrl
            });
          }
        });
      }
    });
  }, [idEdit, setValue]);

  useEffect(() => {
    register('body');
  });
  const watchVideo = watch(['media']);

  const handleGetDataFromEditor = (data: string) => {
    setValue('body', data);
    trigger('body');
  };

  const removeFile = (name: string) => {
    setValue(name, undefined);
  };

  const removeLocalMedia = (name, slot) => {
    let fileName = name.slice(name.lastIndexOf('/') + 1);
    let arr = listDelete;
    arr.push({ id: idEdit, fileName: fileName });
    setListDelete(arr);
    setAllMedia({ ...allMedia, [slot]: '' });
  };

  return (
    <Dialog maxWidth="md" fullWidth={true} onClose={onClose} open={open}>
      <DialogTitle>Edit</DialogTitle>
      <Dialog maxWidth="md" fullWidth={true} onClose={onClose} open={open}>
        <DialogTitle>
          {' '}
          <Typography
            variant="h3"
            sx={{ fontWeight: 'bold', textTransform: 'uppercase' }}
          >
            Edit Screensaver
          </Typography>
        </DialogTitle>
        <DialogContent sx={{ width: { md: '100%', xs: '100%', sm: '100%' } }}>
          <Box component="form" onSubmit={handleSubmit(onSubmit)}>
            <Box sx={{ mt: 3 }}>
              <TextField
                required
                id="outlined-basic"
                label="Title"
                variant="outlined"
                error={errors.title}
                {...register('title', {
                  required: 'Title is require',
                  maxLength: {
                    value: 80,
                    message: 'Max length is 80 characters'
                  }
                })}
                sx={{ width: '100%' }}
              />
            </Box>
            {errors.title && (
              <Typography color="error">{errors.title.message}</Typography>
            )}
            <Box sx={{ mt: 3 }}>
              <Typography>
                Body text <span style={{ color: 'red' }}>*Optional*</span> (max.
                300 characters)
              </Typography>

              <Jodit
                initialValue={getValues('body') as string}
                limit={300}
                disable={false}
                handleGetDataFromEditor={handleGetDataFromEditor}
              />
            </Box>

            <Box sx={{ mt: 3 }}>
              <Typography>
                Screensaver (9:16, 1080 x 1920 px, JPG/PNG max. 5MB)
              </Typography>
              <Button variant="contained" component="label" color="secondary">
                Upload File
                <input
                  type="file"
                  hidden
                  {...register('media')}
                  accept=".jpg, .png"
                />
              </Button>
              {watchVideo[0] && watchVideo[0][0] && (
                <Box
                  sx={{
                    mt: 3,
                    display: 'flex',
                    justifyContent: 'space-between'
                  }}
                >
                  <Typography sx={{ fontStyle: 'italic', mt: 3 }}>
                    {handleUploadFile(watchVideo[0][0])}
                  </Typography>
                  <Typography>
                    <CloseIcon
                      color="error"
                      style={{ cursor: 'pointer' }}
                      onClick={() => {
                        removeFile('media');
                      }}
                    />
                  </Typography>
                </Box>
              )}

              {(!watchVideo[0] ||
                watchVideo[0] === undefined ||
                watchVideo[0].length === 0) &&
                allMedia.mediaUrl && (
                  <Box
                    sx={{
                      mt: 3,
                      display: 'flex',
                      justifyContent: 'space-between'
                    }}
                  >
                    <Typography>
                      <img
                        src={allMedia.mediaUrl}
                        alt="media"
                        width="200"
                        height="auto"
                      />
                    </Typography>

                    <CloseIcon
                      color="error"
                      style={{ cursor: 'pointer' }}
                      onClick={() => {
                        removeLocalMedia(allMedia.mediaUrl, 'mediaUrl');
                      }}
                    />
                  </Box>
                )}
            </Box>
            <Box sx={{ mt: 3 }}>
              <Button
                color="secondary"
                type="submit"
                variant="contained"
                sx={{ width: '100%' }}
              >
                {loading ? <CircularProgress color="info" /> : 'Submit'}
              </Button>
            </Box>
          </Box>
        </DialogContent>
      </Dialog>
    </Dialog>
  );
}

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  idEdit: PropTypes.string.isRequired,
  handleOpenToast: PropTypes.func.isRequired,
  changeToastMessage: PropTypes.func.isRequired
};
interface PropsId {
  id: string;
}
function Edit({ id }: PropsId) {
  const [open, setOpen] = useState(false);
  const [openToast, setOpenToast] = useState(false);
  const [msg, setMsg] = useState<string>('');

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleCloseToast = () => {
    setOpenToast(false);
  };

  const handleOpenToast = () => {
    setOpenToast(true);
  };

  const changeToastMessage = (msg: string) => {
    setMsg(msg);
  };
  return (
    <>
      <EditTwoToneIcon fontSize="small" onClick={handleClickOpen} />

      <SimpleDialog
        open={open}
        onClose={handleClose}
        idEdit={id}
        changeToastMessage={changeToastMessage}
        handleOpenToast={handleOpenToast}
      />
      <Snackbar
        autoHideDuration={3000}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={openToast}
        onClose={handleCloseToast}
        message={msg}
        key={'top right'}
      />
    </>
  );
}

export default Edit;
