import { Card, LinearProgress } from '@mui/material';
import { useContext, useEffect, useState } from 'react';
import corridorBApi from 'src/api/corridorBApi';
import station from 'src/api/station';
import { AuthContext } from 'src/App';
import { Corridor } from 'src/models';
import RecentOrdersTable from './RecentOrdersTable';

function RecentOrders() {
  const { updated } = useContext(AuthContext);
  const [loading, setLoading] = useState<boolean>(false);
  useEffect(() => {
    setLoading(true);
    station.getSation2().then((res) => {
      if (res.data.success) {
        corridorBApi.getCorridorB(res.data.data.id).then((res) => {
          if (res.data.success) {
            setLoading(false);
            setCryptoOrders(res.data.data);
          }
        });
      }
    });
  }, [updated]);
  const [cryptoOrders, setCryptoOrders] = useState<Corridor[]>([]);

  return (
    <Card>
      <RecentOrdersTable cryptoOrders={cryptoOrders} />
      {loading && <LinearProgress />}
    </Card>
  );
}

export default RecentOrders;
