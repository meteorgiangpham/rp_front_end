import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import AddTwoToneIcon from '@mui/icons-material/AddTwoTone';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import {
  Box,
  DialogContent,
  TextField,
  Typography,
  Snackbar,
  CircularProgress
} from '@mui/material';
import { useForm } from 'react-hook-form';
import { DateTimePicker } from '@mui/lab';
import corridorBApi from 'src/api/corridorBApi';
import { format } from 'date-fns';

function SimpleDialog(props) {
  const { onClose, open, onOpenToast, changeToastMessage } = props;
  const [loading, setLoading] = useState<boolean>(false);

  // React-hook-form
  const { register, handleSubmit, setValue, trigger, watch } = useForm({
    mode: 'onChange'
  });
  const onSubmit = async (data) => {
    const { title, body, timer, media } = data;
    setLoading(true);
    if (media[0]) {
      const formData = new FormData();
      formData.append('StationId', '894651b3-e170-4015-691d-08da08bc6dfd');
      formData.append('Title', title);
      formData.append('Text', body);
      formData.append('Media', media[0]);

      formData.append('Timer', format(timer, 'yyyy-MM-dd h:mm:ss').toString());

      try {
        await corridorBApi.addCorridorB(formData).then((res) => {
          setLoading(false);

          if (res.data.success) {
            onClose();
            changeToastMessage(res.data.message);
            onOpenToast();
          } else {
            changeToastMessage(res.data.message);
            onOpenToast();
          }
        });
      } catch (error) {
        setLoading(false);
      }
    }
  };
  useEffect(() => {
    register('body');
    register('timer');
    setValue('timer', new Date());
  });
  const watchVideo = watch(['media']);

  const [timer, setTimer] = useState<Date | null>(new Date());
  const handleUploadFile = (file: File) => {
    if (
      file.type.toLowerCase() === 'image/jpg' ||
      file.type.toLowerCase() === 'image/jpeg' ||
      file.type.toLowerCase() === 'video/mp4'
    )
      return true;
    else return false;
  };
  const handleChange = (newValue: Date | null) => {
    setValue('timer', newValue);
    trigger('timer');
    setTimer(newValue);
  };

  return (
    <Dialog maxWidth="md" fullWidth={true} onClose={onClose} open={open}>
      <DialogTitle>
        <Typography
          variant="h3"
          sx={{ fontWeight: 'bold', textTransform: 'uppercase' }}
        >
          Add Screensaver
        </Typography>
      </DialogTitle>
      <DialogContent sx={{ width: { md: '100%', xs: '100%', sm: '100%' } }}>
        <Box component="form" onSubmit={handleSubmit(onSubmit)}>
          <Box sx={{ mt: 3 }}>
            <TextField
              required
              id="outlined-basic"
              label="Title"
              variant="outlined"
              {...register('title', { required: true, maxLength: 70 })}
              sx={{ width: '100%' }}
            />
          </Box>
          <Box sx={{ mt: 3 }}>
            <CKEditor
              editor={ClassicEditor}
              onChange={(event, editor) => {
                setValue('body', editor.getData());
                trigger('body');
              }}
            />
          </Box>
          <Box sx={{ mt: 3 }}>
            <DateTimePicker
              label="Timer"
              value={timer}
              onChange={handleChange}
              renderInput={(params) => <TextField {...params} />}
            />
          </Box>
          <Box sx={{ mt: 3 }}>
            <Typography>Media url</Typography>
            <Button variant="contained" component="label">
              Upload File
              <input
                type="file"
                hidden
                {...register('media')}
                accept=".jpg, .mp4"
                required
              />
            </Button>
            {watchVideo[0] &&
              watchVideo[0][0] &&
              (handleUploadFile(watchVideo[0][0]) ? (
                <Typography sx={{ fontStyle: 'italic' }}>
                  {watchVideo[0][0].name}
                </Typography>
              ) : (
                <Typography sx={{ fontStyle: 'italic', color: 'red' }}>
                  Incorrect format
                </Typography>
              ))}
          </Box>
          <Box sx={{ mt: 3 }}>
            <Button
              color="secondary"
              type="submit"
              variant="contained"
              sx={{ width: '100%' }}
            >
              {loading ? <CircularProgress color="info" /> : 'Submit'}
            </Button>
          </Box>
        </Box>
      </DialogContent>
    </Dialog>
  );
}

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  onOpenToast: PropTypes.func.isRequired,
  changeToastMessage: PropTypes.func.isRequired
};

function Add() {
  const [open, setOpen] = useState(false);
  const [openToast, setOpenToast] = useState(false);
  const [msg, setMsg] = useState<string>('');
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleCloseToast = () => {
    setOpenToast(false);
  };

  const handleOpenToast = () => {
    setOpenToast(false);
  };

  const changeToastMessage = (msg: string) => {
    setMsg(msg);
  };

  return (
    <>
      <Button
        onClick={handleClickOpen}
        sx={{ mt: { xs: 2, md: 0 } }}
        variant="contained"
        startIcon={<AddTwoToneIcon fontSize="small" />}
      >
        Create
      </Button>
      <SimpleDialog
        open={open}
        onClose={handleClose}
        onOpenToast={handleOpenToast}
        changeToastMessage={changeToastMessage}
      />

      <Snackbar
        autoHideDuration={3000}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={openToast}
        onClose={handleCloseToast}
        message={msg}
        key={'top' + 'right'}
      />
    </>
  );
}

export default Add;
