import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import {
  Box,
  DialogContent,
  Snackbar,
  TextField,
  Typography
} from '@mui/material';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import PropTypes from 'prop-types';
import { useContext, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import corridorBApi from 'src/api/corridorBApi';
import station from 'src/api/station';
import { AuthContext } from 'src/App';
import Jodit from 'src/components/TinyEditor/JoditEditor';

function SimpleDialog(props) {
  const { updateSuccess } = useContext(AuthContext);
  const {
    onClose,
    open,
    idEdit,
    disable,
    handleOpenToast,
    changeToastMessage
  } = props;

  const [allMedia, setAllMedia] = useState({
    ledVideoUrl: '',
    mediaUrl: '',
    thumbnailImageUrl: ''
  });
  const { register, handleSubmit, getValues, setValue, trigger, watch } =
    useForm({
      mode: 'onChange'
    });

  const onSubmit = async (data) => {
    const { title, body, media } = data;

    const formData = new FormData();
    formData.append('Id', idEdit);
    formData.append('Title', title);
    formData.append('Text', body);
    if (media[0]) {
      formData.append('Media', media[0]);
    }

    try {
      await corridorBApi.editCorridorB(formData).then((res) => {
        if (res.data.success) {
          onClose();
          handleOpenToast();
          changeToastMessage(res.data.message);
          updateSuccess();
        } else {
          changeToastMessage(res.data.message);
          handleOpenToast();
        }
      });
    } catch (error) {}
  };

  const handleUploadFile = (file: File) => {
    if (
      file.type.toLowerCase() === 'image/jpg' ||
      file.type.toLowerCase() === 'image/jpeg' ||
      file.type.toLowerCase() === 'image/png'
    ) {
      if (file.size <= 5012 * 1024) {
        return <p>{file.name}</p>;
      } else return <p style={{ color: 'red' }}>This file is too large</p>;
    } else return <p style={{ color: 'red' }}>Incorrect format</p>;
  };

  useEffect(() => {
    station.getSation2().then((res) => {
      if (res.data.success) {
        corridorBApi.getCorridorBById(res.data.data.id, idEdit).then((res) => {
          if (res.data.success) {
            const data = res.data.data;

            setValue('title', data.title);
            setValue('body', data.text);

            setAllMedia({
              ledVideoUrl: data.ledVideoUrl,
              mediaUrl: data.mediaUrl,
              thumbnailImageUrl: data.thumbnailImageUrl
            });
          }
        });
      }
    });
  }, [idEdit, setValue]);

  useEffect(() => {
    register('body');
  });
  const watchVideo = watch(['media']);

  const handleGetDataFromEditor = (data: string) => {
    setValue('body', data);
    trigger('body');
  };

  return (
    <Dialog maxWidth="md" fullWidth={true} onClose={onClose} open={open}>
      <DialogTitle>Edit</DialogTitle>
      <Dialog maxWidth="md" fullWidth={true} onClose={onClose} open={open}>
        <DialogTitle>
          {' '}
          <Typography
            variant="h3"
            sx={{ fontWeight: 'bold', textTransform: 'uppercase' }}
          >
            View Detail Screensaver
          </Typography>
        </DialogTitle>
        <DialogContent sx={{ width: { md: '100%', xs: '100%', sm: '100%' } }}>
          <Box component="form" onSubmit={handleSubmit(onSubmit)}>
            <Box sx={{ mt: 3 }}>
              <TextField
                required
                id="outlined-basic"
                label="Title"
                disabled={disable}
                variant="outlined"
                {...register('title', { required: true, maxLength: 70 })}
                sx={{ width: '100%' }}
              />
            </Box>
            <Box sx={{ mt: 3 }}>
              <Typography>
                Body text <span style={{ color: 'red' }}>*Optional*</span> (max.
                300 characters)
              </Typography>
              <Jodit
                initialValue={getValues('body') as string}
                limit={300}
                disable={true}
                handleGetDataFromEditor={handleGetDataFromEditor}
              />
            </Box>

            <Box sx={{ mt: 3 }}>
              <Typography>
                Screensaver (9:16, 1080 x 1920 px, JPG/PNG max. 5MB)
              </Typography>

              {watchVideo[0] && watchVideo[0][0] && (
                <Typography sx={{ fontStyle: 'italic', mt: 3 }}>
                  {handleUploadFile(watchVideo[0][0])}
                </Typography>
              )}
              {!watchVideo[0] && allMedia.mediaUrl && (
                <Typography>
                  <img
                    src={allMedia.mediaUrl}
                    alt="media"
                    width="200"
                    style={{ objectFit: 'contain' }}
                    height="auto"
                  />
                </Typography>
              )}
            </Box>
          </Box>
        </DialogContent>
      </Dialog>
    </Dialog>
  );
}

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  idEdit: PropTypes.string.isRequired,
  handleOpenToast: PropTypes.func.isRequired,
  changeToastMessage: PropTypes.func.isRequired,
  disable: PropTypes.bool.isRequired
};
interface PropsId {
  id: string;
}
function Edit({ id }: PropsId) {
  const [open, setOpen] = useState(false);
  const [openToast, setOpenToast] = useState(false);
  const [msg, setMsg] = useState<string>('');

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleCloseToast = () => {
    setOpenToast(false);
  };

  const handleOpenToast = () => {
    setOpenToast(true);
  };

  const changeToastMessage = (msg: string) => {
    setMsg(msg);
  };
  return (
    <>
      <RemoveRedEyeIcon fontSize="small" onClick={handleClickOpen} />

      <SimpleDialog
        open={open}
        onClose={handleClose}
        idEdit={id}
        changeToastMessage={changeToastMessage}
        handleOpenToast={handleOpenToast}
        disable={true}
      />
      <Snackbar
        autoHideDuration={3000}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={openToast}
        onClose={handleCloseToast}
        message={msg}
        key={'top right'}
      />
    </>
  );
}

export default Edit;
