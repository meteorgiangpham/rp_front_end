import PropTypes from 'prop-types';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import { useContext, useEffect, useState } from 'react';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';

import { Box, DialogContent, Snackbar, Typography } from '@mui/material';
import { useForm } from 'react-hook-form';

import station from 'src/api/station';
import CloseIcon from '@mui/icons-material/Close';
import { AuthContext } from 'src/App';
import facadeLed from 'src/api/facadeLed';
function SimpleDialog(props) {
  const { updateSuccess } = useContext(AuthContext);
  const {
    onClose,
    open,

    idEdit,
    handleOpenToast,
    changeToastMessage
  } = props;

  const [allMedia, setAllMedia] = useState({
    ledVideoUrl: ''
  });
  // React-hook-form

  const { handleSubmit, setValue, watch } = useForm({
    mode: 'onChange'
  });
  const onSubmit = async (data) => {
    const { video } = data;
    const formData = new FormData();
    formData.append('Id', idEdit);

    if (video[0] && video[0].type.toLowerCase() === 'video/mp4') {
      formData.append('LedVideo', video[0]);
    }

    try {
      await facadeLed.editLedById(formData).then((res) => {
        if (res.data.success) {
          onClose();
          handleOpenToast();
          changeToastMessage(res.data.message);
          updateSuccess();
        } else {
          changeToastMessage(res.data.message);

          handleOpenToast();
        }
      });
    } catch (error) {}
  };
  useEffect(() => {
    station.getSation().then((res) => {
      if (res.data.success) {
        facadeLed.getLedById(res.data.data.id, idEdit).then((res) => {
          if (res.data.success) {
            const data = res.data.data;
            setAllMedia({
              ledVideoUrl: data.ledVideoUrl
            });
          }
        });
      }
    });
  }, [idEdit, setValue]);

  const handleImageOrVideo = (file: File) => {
    if (file) {
      if (
        file.type.toLowerCase() === 'image/jpg' ||
        file.type.toLowerCase() === 'image/jpeg' ||
        file.type.toLowerCase() === 'image/png'
      ) {
        if (file.size <= 5012 * 1024) return <p>{file.name}</p>;
        else return <p style={{ color: 'red' }}>This file is too large</p>;
      } else if (file.type.toLowerCase() === 'video/mp4') {
        if (file.size <= 2147483648) return <p>{file.name}</p>;
        else return <p style={{ color: 'red' }}>This file is too large</p>;
      } else {
        return <p style={{ color: 'red' }}>Incorrect format</p>;
      }
    }
  };

  const watchVideo = watch(['video']);

  const removeFile = (name: string) => {
    setValue(name, undefined);
  };
  return (
    <Dialog maxWidth="md" fullWidth={true} onClose={onClose} open={open}>
      <DialogTitle>
        {' '}
        <Typography
          variant="h3"
          sx={{ fontWeight: 'bold', textTransform: 'uppercase' }}
        >
          View Detail LED Screensaver
        </Typography>
      </DialogTitle>
      <DialogContent sx={{ width: '100%' }}>
        <Box component="form" onSubmit={handleSubmit(onSubmit)}>
          <Box sx={{ mt: 3 }}>
            <Typography>
              LED Panel Screensaver (16:9, 1920 x 1080 px, MP4 max. 2GB)
            </Typography>

            {watchVideo[0] &&
              watchVideo[0][0] &&
              (handleImageOrVideo(watchVideo[0][0]) ? (
                <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
                  <Typography sx={{ fontStyle: 'italic' }}>
                    {watchVideo[0][0].name}
                  </Typography>
                  <Typography>
                    <CloseIcon
                      onClick={() => {
                        removeFile('video');
                      }}
                    />
                  </Typography>
                </Box>
              ) : (
                <Typography sx={{ fontStyle: 'italic', color: 'red' }}>
                  Incorrect format
                </Typography>
              ))}
            {(!watchVideo[0] || !watchVideo[0][0]) && allMedia.ledVideoUrl && (
              <Typography sx={{ mt: 3 }}>
                <video
                  src={allMedia.ledVideoUrl}
                  width="200"
                  height="125"
                  controls
                ></video>
              </Typography>
            )}
          </Box>
        </Box>
      </DialogContent>
    </Dialog>
  );
}

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  idEdit: PropTypes.string.isRequired,
  handleOpenToast: PropTypes.func.isRequired,
  disable: PropTypes.bool.isRequired,
  changeToastMessage: PropTypes.func.isRequired
};

interface PropsId {
  id: string;
}
function Edit({ id }: PropsId) {
  const [open, setOpen] = useState(false);
  const [openToast, setOpenToast] = useState(false);
  const [msg, setMsg] = useState<string>('');
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleCloseToast = () => {
    setOpenToast(false);
  };

  const handleOpenToast = () => {
    setOpenToast(true);
  };

  const changeToastMessage = (msg: string) => {
    setMsg(msg);
  };
  return (
    <>
      <RemoveRedEyeIcon fontSize="small" onClick={handleClickOpen} />

      <SimpleDialog
        open={open}
        onClose={handleClose}
        idEdit={id}
        disable={true}
        changeToastMessage={changeToastMessage}
        handleOpenToast={handleOpenToast}
      />
      <Snackbar
        autoHideDuration={3000}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={openToast}
        onClose={handleCloseToast}
        message={msg}
        key={'top right'}
      />
    </>
  );
}

export default Edit;
