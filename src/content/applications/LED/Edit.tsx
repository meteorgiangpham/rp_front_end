import CloseIcon from '@mui/icons-material/Close';
import EditTwoToneIcon from '@mui/icons-material/EditTwoTone';
import {
  Box,
  Button,
  CircularProgress,
  DialogContent,
  Snackbar,
  Typography
} from '@mui/material';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import PropTypes from 'prop-types';
import { useContext, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import facadeLed from 'src/api/facadeLed';
import station from 'src/api/station';
import { AuthContext } from 'src/App';
import { PreviewImage } from 'src/components/Common/PreviewImage';

function SimpleDialog(props) {
  const { updateSuccess } = useContext(AuthContext);
  const { onClose, open, idEdit, handleOpenToast, changeToastMessage } = props;
  const [loading, setLoading] = useState<boolean>(false);
  const [allMedia, setAllMedia] = useState({
    ledVideoUrl: ''
  });
  const [listDelete, setListDelete] = useState<any>([]);
  // React-hook-form

  const { register, handleSubmit, setValue, watch } = useForm({
    mode: 'onChange'
  });
  const onSubmit = async (data) => {
    setLoading(true);
    const { video } = data;
    const formData = new FormData();
    formData.append('Id', idEdit);

    if (
      video !== undefined &&
      video[0] !== undefined &&
      video[0].type.toLowerCase() === 'video/mp4'
    ) {
      formData.append('LedVideo', video[0]);
    }

    if (listDelete.length > 0) {
      listDelete.forEach((item, index) => {
        formData.append(`ImageDeletes[${index}].Id`, item.id);
        formData.append(`ImageDeletes[${index}].FileName`, item.fileName);
      });
    }
    try {
      await facadeLed.editLedById(formData).then((res) => {
        setLoading(false);

        if (res.data.success) {
          onClose();
          handleOpenToast();
          changeToastMessage(res.data.message);
          updateSuccess();
          setValue('video', undefined);
        } else {
          changeToastMessage(res.data.message);

          handleOpenToast();
        }
      });
    } catch (error) {
      setLoading(false);
    }
  };
  useEffect(() => {
    station.getSation().then((res) => {
      if (res.data.success) {
        facadeLed.getLedById(res.data.data.id, idEdit).then((res) => {
          if (res.data.success) {
            const data = res.data.data;
            setAllMedia({
              ledVideoUrl: data.ledVideoUrl ? data.ledVideoUrl : ''
            });
          }
        });
      }
    });
  }, [idEdit, setValue]);

  const handleImageOrVideo = (file: File) => {
    if (file) {
      if (
        file.type.toLowerCase() === 'image/jpg' ||
        file.type.toLowerCase() === 'image/jpeg' ||
        file.type.toLowerCase() === 'image/png'
      ) {
        if (file.size <= 5012 * 1024)
          return <PreviewImage selectedFile={file} />;
        else return <p style={{ color: 'red' }}>This file is too large</p>;
      } else if (file.type.toLowerCase() === 'video/mp4') {
        if (file.size <= 2147483648)
          return <PreviewImage selectedFile={file} />;
        else return <p style={{ color: 'red' }}>This file is too large</p>;
      } else {
        return <p style={{ color: 'red' }}>Incorrect format</p>;
      }
    }
  };

  const watchVideo = watch(['video']);

  const removeFile = (name: string) => {
    setValue(name, undefined);
  };

  const removeLocalMedia = (name, slot) => {
    let fileName = name.slice(name.lastIndexOf('/') + 1);
    let arr = listDelete;
    arr.push({ id: idEdit, fileName: fileName });
    setListDelete(arr);
    setAllMedia({ ...allMedia, [slot]: '' });
  };
  return (
    <Dialog maxWidth="md" fullWidth={true} onClose={onClose} open={open}>
      <DialogTitle>
        {' '}
        <Typography
          variant="h3"
          sx={{ fontWeight: 'bold', textTransform: 'uppercase' }}
        >
          Edit LED Screensaver
        </Typography>
      </DialogTitle>
      <DialogContent sx={{ width: '100%' }}>
        <Box component="form" onSubmit={handleSubmit(onSubmit)}>
          <Box sx={{ mt: 3 }}>
            <Typography>
              LED Panel Screensaver (16:9, 1920 x 1080 px, MP4 max. 2GB)
            </Typography>
            <Button variant="contained" component="label" color="secondary">
              Upload File
              <input type="file" hidden {...register('video')} accept=".mp4" />
            </Button>

            {watchVideo[0] && watchVideo[0][0] && (
              <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
                <Typography sx={{ fontStyle: 'italic' }}>
                  {handleImageOrVideo(watchVideo[0][0])}
                </Typography>
                <Typography>
                  <CloseIcon
                    color="error"
                    style={{ cursor: 'pointer' }}
                    onClick={() => {
                      removeFile('video');
                    }}
                  />
                </Typography>
              </Box>
            )}
            {(!watchVideo[0] || !watchVideo[0][0]) && allMedia.ledVideoUrl && (
              <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
                <Typography sx={{ mt: 3 }}>
                  <video
                    src={allMedia.ledVideoUrl}
                    width="200"
                    height="125"
                    controls
                  ></video>
                </Typography>
                <CloseIcon
                  color="error"
                  style={{ cursor: 'pointer' }}
                  onClick={() => {
                    removeLocalMedia(allMedia.ledVideoUrl, 'ledVideoUrl');
                  }}
                />
              </Box>
            )}
          </Box>

          <Box sx={{ mt: 3 }}>
            <Button
              color="secondary"
              type="submit"
              variant="contained"
              sx={{ width: '100%' }}
            >
              {loading ? <CircularProgress color="info" /> : 'Submit'}
            </Button>
          </Box>
        </Box>
      </DialogContent>
    </Dialog>
  );
}

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  idEdit: PropTypes.string.isRequired,
  handleOpenToast: PropTypes.func.isRequired,
  changeToastMessage: PropTypes.func.isRequired
};

interface PropsId {
  id: string;
}
function Edit({ id }: PropsId) {
  const [open, setOpen] = useState(false);
  const [openToast, setOpenToast] = useState(false);
  const [msg, setMsg] = useState<string>('');
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleCloseToast = () => {
    setOpenToast(false);
  };

  const handleOpenToast = () => {
    setOpenToast(true);
  };

  const changeToastMessage = (msg: string) => {
    setMsg(msg);
  };
  return (
    <>
      <EditTwoToneIcon fontSize="small" onClick={handleClickOpen} />

      <SimpleDialog
        open={open}
        onClose={handleClose}
        idEdit={id}
        changeToastMessage={changeToastMessage}
        handleOpenToast={handleOpenToast}
      />
      <Snackbar
        autoHideDuration={3000}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={openToast}
        onClose={handleCloseToast}
        message={msg}
        key={'top right'}
      />
    </>
  );
}

export default Edit;
