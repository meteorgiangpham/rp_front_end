import { Card, LinearProgress } from '@mui/material';
import { useContext, useEffect, useState } from 'react';
import facadeLed from 'src/api/facadeLed';
import station from 'src/api/station';
import { AuthContext } from 'src/App';
import { Led } from 'src/models/led';
import RecentOrdersTable from './RecentOrdersTable';

function RecentOrders() {
  const [loading, setLoading] = useState<boolean>(false);
  const { updated } = useContext(AuthContext);
  useEffect(() => {
    station.getSation().then((res) => {
      setLoading(true);
      if (res.data.success) {
        facadeLed.getLed(res.data.data.id).then((res) => {
          if (res.data.success) {
            setLoading(false);
            setCryptoOrders(res.data.data);
          }
        });
      }
    });
  }, [updated]);
  const [cryptoOrders, setCryptoOrders] = useState<Led[]>([]);

  return (
    <Card>
      <RecentOrdersTable cryptoOrders={cryptoOrders} />
      {loading && <LinearProgress />}
    </Card>
  );
}

export default RecentOrders;
