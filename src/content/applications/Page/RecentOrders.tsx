import { Card, LinearProgress } from '@mui/material';
import { useContext, useEffect, useState } from 'react';
import page from 'src/api/page';
import { AuthContext } from 'src/App';
import { Page } from 'src/models';
import RecentOrdersTable from './RecentOrdersTable';

function RecentOrders() {
  const [loading, setLoading] = useState<boolean>(false);
  const { updated } = useContext(AuthContext);
  useEffect(() => {
    setLoading(true);
    page.getPage().then((res) => {
      if (res.data.success) {
        setLoading(false);
        setCryptoOrders(res.data.data);
      }
    });
  }, [updated]);
  const [cryptoOrders, setCryptoOrders] = useState<Page[]>([]);

  return (
    <Card>
      <RecentOrdersTable cryptoOrders={cryptoOrders} />
      {loading && <LinearProgress />}
    </Card>
  );
}

export default RecentOrders;
