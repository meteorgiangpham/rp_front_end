import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import CloseIcon from '@mui/icons-material/Close';
import EditTwoToneIcon from '@mui/icons-material/EditTwoTone';
import {
  Box,
  Button,
  CircularProgress,
  DialogContent,
  Snackbar,
  Switch,
  TextField,
  Typography
} from '@mui/material';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import PropTypes from 'prop-types';
import { useContext, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import page from 'src/api/page';
import { AuthContext } from 'src/App';
import { PreviewImage } from 'src/components/Common/PreviewImage';
import ContentBlock from './ContentBlock';

interface IFileName {
  mediaUrl: string | null;
}
function SimpleDialog(props) {
  const { onClose, open, id, handleOpenToast, changeToastMessage, index } =
    props;
  const { updateSuccess, updated } = useContext(AuthContext);
  const [checked, setChecked] = useState<boolean>(true);
  const [checkedBlock, setCheckedBlock] = useState<Array<boolean>>([
    true,
    true,
    true
  ]);
  const [loading, setLoading] = useState<boolean>(false);
  const [allMedia, setAllMedia] = useState({
    backgroundImageUrl: ''
  });
  const [listDelete, setListDelete] = useState<any>([]);

  const [listContentBlockDelete, setListContentBlockDelete] = useState<any>([]);
  const [allMediaContentBlock, setAllMediaContentBlock] = useState<
    Array<IFileName[]>
  >([]);

  const [contentBlock, setContentBlock] = useState<number>(0);
  const getNameInUrl = (url: string) => {
    return url && url.split('/')[url.split('/').length - 1];
  };
  const {
    register,
    getValues,
    handleSubmit,
    setValue,
    trigger,
    watch,
    formState: { errors }
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange'
  });
  const onSubmit = async (data) => {
    setLoading(true);

    const {
      title,
      subtitle,
      background,
      description,
      visible,
      body1,
      body2,
      body3,
      mediaContent1,
      mediaContent2,
      mediaContent3,
      mediaContentBlock1,
      mediaContentBlock2,
      mediaContentBlock3,
      mediaContentThumbnail1,
      mediaContentThumbnail2,
      mediaContentThumbnail3,
      idBlock1,
      idBlock2,
      idBlock3,
      headerContentBlock1,
      headerContentBlock2,
      headerContentBlock3,
      mediaContentBlockOld1,
      mediaContentBlockOld2,
      mediaContentBlockOld3,
      visible1,
      visible2,
      visible3
    } = data;

    const formData = new FormData();
    formData.append('Id', id);
    formData.append('Title', title);
    formData.append('SubTitle', subtitle);
    formData.append('Description', description);

    formData.append('IsVisible', visible);

    const lstContentBlocks = [
      {
        header: headerContentBlock1,
        description: body1,
        isShowMedia: visible1,
        thumbnail: mediaContentThumbnail1 ? mediaContentThumbnail1[0] : null,
        media: mediaContent1 ? mediaContent1 : null,
        thumbnails: mediaContentBlock1 ? mediaContentBlock1 : [],
        thumbnailsOld: mediaContentBlockOld1 ? mediaContentBlockOld1 : [],

        id: idBlock1
      },
      {
        header: headerContentBlock2,
        description: body2,
        thumbnail: mediaContentThumbnail2 ? mediaContentThumbnail2[0] : null,
        isShowMedia: visible2,
        media: mediaContent2 ? mediaContent2 : null,
        thumbnails: mediaContentBlock2 ? mediaContentBlock2 : [],
        thumbnailsOld: mediaContentBlockOld2 ? mediaContentBlockOld2 : [],
        id: idBlock2
      },
      {
        header: headerContentBlock3,
        description: body3,
        isShowMedia: visible3,
        thumbnail: mediaContentThumbnail3 ? mediaContentThumbnail3[0] : null,
        media: mediaContent3 ? mediaContent3 : null,
        id: idBlock3,
        thumbnailsOld: mediaContentBlockOld3 ? mediaContentBlockOld3 : [],
        thumbnails: mediaContentBlock3 ? mediaContentBlock3 : []
      }
    ];

    if (listContentBlockDelete.length > 0) {
      listContentBlockDelete.forEach((item, index) => {
        formData.append(`ContentBlockDeletes[${index}].PageId`, item.pageId);
        formData.append(
          `ContentBlockDeletes[${index}].ContentBlockId`,
          item.contentBlockId
        );
      });
    }

    if (listDelete.length > 0) {
      listDelete.forEach((item, index) => {
        formData.append(`ImageDeletes[${index}].Id`, item.id);
        formData.append(`ImageDeletes[${index}].FileName`, item.fileName);
      });
    }

    lstContentBlocks.forEach((item, index) => {
      if (index <= contentBlock - 1) {
        const listImage = item.thumbnails.concat(item.thumbnailsOld);

        listImage &&
          listImage.forEach((d, i) => {
            if ('media' in d) {
              formData.append(
                `ContentBlocks[${index}].MediaThumbnails[${i}].Media`,
                d.media
              );

              formData.append(
                `ContentBlocks[${index}].MediaThumbnails[${i}].MediaUrl`,
                ''
              );
            } else {
              formData.append(
                `ContentBlocks[${index}].MediaThumbnails[${i}].MediaUrl`,
                d.mediaUrl !== '' ? getNameInUrl(d.mediaUrl) : ''
              );
            }
          });
        formData.append(
          `ContentBlocks[${index}].Header`,
          item.header ? item.header : ' '
        );
        formData.append(
          `ContentBlocks[${index}].Description`,
          item.description
        );
        if (item.id) {
          formData.append(`ContentBlocks[${index}].Id`, item.id);
        }

        if (item.isShowMedia) {
          formData.append(
            `ContentBlocks[${index}].IsShowMedia`,
            item.isShowMedia
          );
        }
      }
    });

    if (background !== undefined && background[0] !== undefined) {
      formData.append('Background', background[0]);
    }

    try {
      await page.editPage(formData).then((res) => {
        setLoading(false);

        if (res.data.success) {
          onClose();
          handleOpenToast();
          changeToastMessage(res.data.message);
          updateSuccess();

          [...Array(3)].forEach((item, index) => {
            setValue(`mediaContent${index + 1}`, undefined);
            setValue(`mediaContentThumbnail${index + 1}`, undefined);
          });
        } else {
          changeToastMessage(res.data.message);

          handleOpenToast();
        }
      });
    } catch (error) {
      setLoading(false);
    }
  };
  useEffect(() => {
    if (open) {
      page.getPageById(id).then((res) => {
        if (res.data.success) {
          const data = res.data.data;

          setValue('title', data.title ? data.title : '');
          setValue('subtitle', data.subTitle ? data.subTitle : '');
          setValue('description', data.description ? data.description : '');

          if (data.contentBlocks && data.contentBlocks.length > 0) {
            let checkBox = [];
            let listMedia = [];

            data.contentBlocks.forEach((d, i) => {
              setValue(`idBlock${i + 1}`, d.id ? d.id : '');
              setValue(`body${i + 1}`, d.description ? d.description : '');
              setValue(`visible${i + 1}`, d.isShowMedia);
              setValue(`headerContentBlock${i + 1}`, d.header ? d.header : '');
              setValue(`mediaContentBlock${i + 1}`, undefined);
              checkBox.push(d.isShowMedia);
              d.listMediaFile !== null && listMedia.push(d.listMediaFile);
            });
            setAllMediaContentBlock(listMedia);
            setCheckedBlock(checkBox);
            updateSuccess();
          }

          setContentBlock(data.contentBlocks.length);
          handleChangeFormOpen(data.isVisible);

          setAllMedia({
            backgroundImageUrl: data.backgroundImageUrl
              ? data.backgroundImageUrl
              : ''
          });
        }
      });
    }
  }, [id, setValue, open]);

  const handleListFile = (file: File) => {
    if (
      file.type.toLowerCase() === 'image/jpg' ||
      file.type.toLowerCase() === 'image/jpeg' ||
      file.type.toLowerCase() === 'image/png'
    ) {
      if (file.size <= 5012 * 1024) return <PreviewImage selectedFile={file} />;
      else return <p style={{ color: 'red' }}>This file is too large</p>;
    } else if (file.type.toLowerCase() === 'video/mp4') {
      if (file.size <= 2147483648) return <PreviewImage selectedFile={file} />;
      else return <p style={{ color: 'red' }}>This file is too large</p>;
    } else {
      return <p style={{ color: 'red' }}>Incorrect format</p>;
    }
  };
  const handleUploadFile = (file: File) => {
    if (
      file.type.toLowerCase() === 'image/jpg' ||
      file.type.toLowerCase() === 'image/jpeg' ||
      file.type.toLowerCase() === 'image/png'
    ) {
      if (file.size <= 5012 * 1024) {
        return <PreviewImage selectedFile={file} />;
      } else return <p style={{ color: 'red' }}>This file is too large</p>;
    } else return <p style={{ color: 'red' }}>Incorrect format</p>;
  };
  useEffect(() => {
    register('body1');
    register('body2');
    register('body3');
    register('visible1');
    register('visible2');
    register('visible3');
    register('mediaContentBlock1');
    register('mediaContentBlock2');
    register('mediaContentBlock3');
    register('mediaContentBlockOld1');
    register('mediaContentBlockOld2');
    register('mediaContentBlockOld3');
    register('idBlock1');
    register('idBlock2');
    register('idBlock3');
  });
  const watchVideo = watch(['background']);

  const handleChange = (event) => {
    setChecked(event.target.checked);
    setValue('visible', event.target.checked);
  };
  const handleChangeBlock = (event) => {
    let index = +event.target.name;
    let temp = checkedBlock;
    temp[index] = Boolean(event.target.checked);

    setCheckedBlock({ ...checkedBlock, ...temp });
    setValue(`visible${index + 1}`, event.target.checked);
  };
  const handleChangeFormOpen = (check: boolean) => {
    setChecked(check);
    setValue('visible', check);
  };

  const removeFile = (name: string) => {
    setValue(name, undefined);
  };

  const minusContentBlock = () => {
    setContentBlock((prev) => prev - 1);
  };
  const removeContentBlock = (contentBlockId) => {
    let arr = listContentBlockDelete;
    arr.push({ pageId: id, contentBlockId: contentBlockId });
    setListContentBlockDelete(arr);
  };

  const removeLocalMediaBackground = (name, slot) => {
    let fileName = name.slice(name.lastIndexOf('/') + 1);
    let arr = listDelete;
    arr.push({ id: id, fileName: fileName });
    setListDelete(arr);

    setAllMedia({ ...allMedia, [slot]: '' });
  };

  const removeImageContentBlock = (name, id) => {
    let fileName = name.slice(name.lastIndexOf('/') + 1);
    let arr = listDelete;
    arr.push({ id: id, fileName: fileName });
    setListDelete(arr);
  };

  return (
    <Dialog maxWidth="md" fullWidth={true} onClose={onClose} open={open}>
      <DialogTitle>{`Edit Page ${index}`}</DialogTitle>
      <Dialog maxWidth="md" fullWidth={true} onClose={onClose} open={open}>
        <DialogTitle>
          {' '}
          <Typography
            variant="h3"
            sx={{ fontWeight: 'bold', textTransform: 'uppercase' }}
          >
            {`Edit Page ${index + 1}`}
          </Typography>
        </DialogTitle>
        <DialogContent sx={{ width: { md: '100%', xs: '100%', sm: '100%' } }}>
          <Box component="form" onSubmit={handleSubmit(onSubmit)}>
            <Box sx={{ mt: 3 }}>
              <TextField
                required
                id="outlined-basic"
                defaultValue="0"
                label="Section header"
                variant="outlined"
                error={errors.title}
                {...register('title', {
                  required: 'Title is require',
                  maxLength: {
                    value: 70,
                    message: 'Max length is 70 characters'
                  }
                })}
                sx={{ width: '100%' }}
              />
            </Box>
            {errors.title && (
              <Typography color="error">{errors.title.message}</Typography>
            )}
            <Box sx={{ mt: 3 }}>
              <TextField
                id="outlined-basic"
                label="Page title"
                defaultValue="0"
                error={Boolean(errors.subtitle)}
                variant="outlined"
                {...register('subtitle', { required: true })}
                sx={{ width: '100%' }}
              />
            </Box>
            <Box sx={{ mt: 3 }}>
              <Typography>
                Page body text <span style={{ color: 'red' }}>*Optional*</span>{' '}
              </Typography>
              <CKEditor
                editor={ClassicEditor}
                data={getValues(`description`)}
                onChange={(event, editor) => {
                  setValue('description', editor.getData());
                  trigger('description');
                }}
              />
            </Box>
            <Box sx={{ mt: 3 }}>
              {contentBlock >= 1 &&
                [...Array(contentBlock)].map((i, index) => (
                  <ContentBlock
                    id={id}
                    key={index + 'content'}
                    index={index}
                    contentBlock={contentBlock}
                    minusContentBlock={minusContentBlock}
                    setValue={setValue}
                    removeContentBlock={removeContentBlock}
                    getValues={getValues}
                    register={register}
                    trigger={trigger}
                    handleChangeBlock={handleChangeBlock}
                    checkedBlock={checkedBlock}
                    handleListFile={handleListFile}
                    allMediaContentBlock={allMediaContentBlock}
                    removeImageContentBlock={removeImageContentBlock}
                  />
                ))}

              <Button
                variant="contained"
                color="secondary"
                sx={{
                  display: `${contentBlock === 3 ? 'none' : 'block'}`,
                  mt: 3
                }}
                onClick={() => {
                  setContentBlock((prev) => prev + 1);
                }}
              >
                Add content block
              </Button>
            </Box>
            <Box sx={{ mt: 3 }}>
              <Typography>
                Background image (9:16, 1080 x 1920 px, JPG/PNG max 5MB)
              </Typography>
              <Button variant="contained" component="label" color="secondary">
                Upload File
                <input
                  type="file"
                  hidden
                  {...register('background')}
                  accept=".jpg, .png, jpeg"
                />
              </Button>
              {watchVideo[0] && watchVideo[0][0] && (
                <Box
                  sx={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    mt: 3
                  }}
                >
                  <Typography sx={{ fontStyle: 'italic' }}>
                    {handleUploadFile(watchVideo[0][0])}
                  </Typography>
                  <Typography>
                    <CloseIcon
                      color="error"
                      style={{ cursor: 'pointer' }}
                      onClick={() => {
                        removeFile('background');
                      }}
                    />
                  </Typography>
                </Box>
              )}
              {(!watchVideo[0] || !watchVideo[0][0]) &&
                allMedia.backgroundImageUrl && (
                  <Box
                    sx={{
                      display: 'flex',
                      justifyContent: 'space-between',
                      mt: 3
                    }}
                  >
                    <Typography>
                      <img
                        src={allMedia.backgroundImageUrl}
                        alt=""
                        width={200}
                      />
                    </Typography>
                    <Typography>
                      <CloseIcon
                        color="error"
                        style={{ cursor: 'pointer' }}
                        onClick={() => {
                          removeLocalMediaBackground(
                            allMedia.backgroundImageUrl,
                            'backgroundImageUrl'
                          );
                        }}
                      />
                    </Typography>
                  </Box>
                )}
            </Box>
            <Box sx={{ mt: 3 }}>
              <Typography>Visible</Typography>
              <Switch
                color="secondary"
                checked={checked}
                onChange={handleChange}
                inputProps={{ 'aria-label': 'controlled' }}
              />
            </Box>
            <Box sx={{ mt: 3 }}>
              <Button
                color="secondary"
                type="submit"
                variant="contained"
                sx={{ width: '100%' }}
              >
                {loading ? <CircularProgress color="info" /> : 'Submit'}
              </Button>
            </Box>
          </Box>
        </DialogContent>
      </Dialog>
    </Dialog>
  );
}

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  id: PropTypes.string.isRequired,
  handleOpenToast: PropTypes.func.isRequired,
  changeToastMessage: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired
};

interface PropsId {
  id: string;
  index: number;
}

function Edit({ id, index }: PropsId) {
  const [open, setOpen] = useState(false);
  const [openToast, setOpenToast] = useState(false);
  const [msg, setMsg] = useState<string>('');
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleCloseToast = () => {
    setOpenToast(false);
  };

  const handleOpenToast = () => {
    setOpenToast(true);
  };

  const changeToastMessage = (msg: string) => {
    setMsg(msg);
  };
  return (
    <>
      <EditTwoToneIcon fontSize="small" onClick={handleClickOpen} />

      <SimpleDialog
        open={open}
        onClose={handleClose}
        id={id}
        changeToastMessage={changeToastMessage}
        handleOpenToast={handleOpenToast}
        index={index}
      />
      <Snackbar
        autoHideDuration={3000}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={openToast}
        onClose={handleCloseToast}
        message={msg}
        key={'top' + 'right'}
      />
    </>
  );
}

export default Edit;
