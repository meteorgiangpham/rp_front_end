import {
  Box,
  Button,
  Grid,
  styled,
  Switch,
  TextField,
  Typography
} from '@mui/material';
import React, { useState, useEffect, useCallback, useContext } from 'react';
import CloseIcon from '@mui/icons-material/Close';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import { useDropzone } from 'react-dropzone';
import CloudUploadOutlinedIcon from '@mui/icons-material/CloudUploadOutlined';
import DeleteForeverRoundedIcon from '@mui/icons-material/DeleteForeverRounded';
import { AuthContext } from 'src/App';

const DropzoneBox = styled(Box)(
  ({ theme }) => `
       height:300px;   
       border: 3px #999;
       border-style: dashed;
       border-radius: 15px;
       display: flex;
       justify-content: center;
       align-items: center
`
);

const ItemImage = styled(Box)(
  ({ theme }) => `
        border-radius: 0;
        margin: ${theme.spacing(1)} 0;
        padding: ${theme.spacing(1)} ${theme.spacing(2)};
        display:flex;
        background:#e9ecef; 
        align-items:center;
`
);
interface IContentBlock {
  index: number;
  contentBlock: number;
  minusContentBlock: () => void;
  setValue: any;
  removeContentBlock: (id: string) => void;
  getValues: any;
  register: any;
  trigger: any;
  checkedBlock: boolean[];
  handleChangeBlock: any;
  allMediaContentBlock: Array<IFileName[]>;
  removeImageContentBlock: (id: string, name: string) => void;
  id: string;
  handleListFile: (file: File) => void;
}

interface IFileName {
  mediaUrl: string | null;
}

interface IFileArray {
  media: File;
  thumbnail: File;
}
function ContentBlock({
  index,
  contentBlock,
  minusContentBlock,
  setValue,
  removeContentBlock,
  getValues,
  register,
  trigger,
  checkedBlock,
  handleChangeBlock,
  allMediaContentBlock,
  id,
  handleListFile,
  removeImageContentBlock
}: IContentBlock) {
  const [myFiles, setMyFiles] = useState<IFileArray[]>([]);
  const [getMyFiles, setGetMyFiles] = useState<IFileName[]>([]);
  const { updated } = useContext(AuthContext);

  const maxSizeVideo = 2;
  const maxSizeImage = 5;
  function sizeValidator(file) {
    if (
      file.type.includes('video') &&
      file.size > maxSizeVideo * 1024 * 1024 * 1024
    )
      return {
        code: 'name-too-large',
        message: `${file.name} is larger than ${maxSizeVideo} GB`
      };
    if (file.type.includes('image') && file.size > maxSizeImage * 1024 * 1024)
      return {
        code: 'name-too-large',
        message: `${file.name} is larger than ${maxSizeImage} MB`
      };
    return null;
  }

  const onDrop = useCallback(
    (acceptedFiles) => {
      let temp = [...myFiles];

      [...acceptedFiles].forEach((d) => {
        temp.push({ media: d, thumbnail: null });
      });

      setMyFiles(temp);
      setValue(`mediaContentBlock${index + 1}`, temp);
    },
    [myFiles]
  );

  const { getRootProps, getInputProps, fileRejections } = useDropzone({
    onDrop,
    maxFiles: 3 - getMyFiles?.length,
    validator: sizeValidator
  });

  const fileRejectionItems = fileRejections.map(({ file, errors }, indmex) => {
    return (
      <Box key={indmex + indmex.toString()}>
        {errors.map((e) => (
          <Typography color="error" key={e.code}>
            {'-'} {e.message}
          </Typography>
        ))}
      </Box>
    );
  });
  const removeFile = (file) => {
    let temp = [...myFiles];
    temp.splice(temp.indexOf(file), 1);
    setMyFiles(temp);
    setValue(`mediaContentBlock${index + 1}`, temp);
  };
  const removeOldFile = (i, name) => {
    let temp = [...getMyFiles];
    temp[i].mediaUrl = '';

    setGetMyFiles(temp);
    setValue(`mediaContentBlockOld${index + 1}`, temp);

    removeImageContentBlock(name, getValues(`idBlock${index + 1}`));
  };

  useEffect(() => {
    setGetMyFiles(
      allMediaContentBlock[index] ? allMediaContentBlock[index] : []
    );
    setValue(`mediaContentBlockOld${index + 1}`, allMediaContentBlock[index]);
  }, [id, updated]);
  const files = myFiles.map((file, i) => (
    <Box key={`${i} 1`}>
      <ItemImage width={'100%'}>
        <Box width={'calc(100% - 150px)'}>
          <Typography
            fontWeight={'bold'}
            fontSize={16}
            sx={{
              '& span': {
                fontWeight: '500',
                fontSize: '14px',
                color: '#999'
              }
            }}
          >
            {file.media.name}
          </Typography>
        </Box>

        <Box width={150} textAlign="right">
          <DeleteForeverRoundedIcon
            sx={{ color: 'red' }}
            onClick={() => {
              removeFile(file);
            }}
          />
        </Box>
      </ItemImage>
      <Box sx={{ display: 'flex' }}>
        <Box width={'calc(100% / 2)'}>
          {file.media.type.includes('video') ? (
            <video
              src={URL.createObjectURL(file.media)}
              width={200}
              controls
            ></video>
          ) : (
            <img src={URL.createObjectURL(file.media)} alt="" width={200} />
          )}
        </Box>
      </Box>
    </Box>
  ));
  const getNameInUrl = (url: string) => {
    return url && url.split('/')[url.split('/').length - 1];
  };
  const showOldFile =
    getMyFiles &&
    getMyFiles.map((file: IFileName, i) => {
      if (file.mediaUrl !== '')
        return (
          <Box key={`${i} 1`}>
            <ItemImage width={'100%'}>
              <Box width={'calc(100% - 150px)'}>
                <Typography
                  fontWeight={'bold'}
                  fontSize={16}
                  sx={{
                    '& span': {
                      fontWeight: '500',
                      fontSize: '14px',
                      color: '#999'
                    }
                  }}
                >
                  {getNameInUrl(file?.mediaUrl)}
                </Typography>
              </Box>

              <Box width={150} textAlign="right">
                <DeleteForeverRoundedIcon
                  sx={{ color: 'red' }}
                  onClick={() => {
                    removeOldFile(i, getNameInUrl(file?.mediaUrl));
                  }}
                />
              </Box>
            </ItemImage>
            <Box sx={{ display: 'flex' }}>
              <Box width={'calc(100% / 2)'}>
                {file && file.mediaUrl && file.mediaUrl.includes('mp4') ? (
                  <video src={file?.mediaUrl} width={200} controls></video>
                ) : (
                  <img src={file?.mediaUrl} alt="" width={200} />
                )}
              </Box>
            </Box>
          </Box>
        );
    });

  return (
    <Box sx={{ mt: 3, border: '2px solid #000', p: 3 }}>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between'
        }}
      >
        <Typography>Content block {index + 1}</Typography>
        <Button
          color="secondary"
          sx={{
            display: `${contentBlock !== index + 1 ? 'none' : 'inherit'}`,
            padding: 0,

            minWidth: 'auto'
          }}
          onClick={() => {
            minusContentBlock();
            setValue(`body${index + 1}`, undefined);
            removeContentBlock(getValues(`idBlock${index + 1}`));
          }}
        >
          <CloseIcon color="error" />
        </Button>
      </Box>
      <Box sx={{ mt: 3 }}>
        <TextField
          label={`Title`}
          variant="outlined"
          {...register(`headerContentBlock${index + 1}`)}
          sx={{ width: '100%' }}
        />
      </Box>

      <Box sx={{ mt: 3 }}>
        <Typography>Body text</Typography>
        <CKEditor
          editor={ClassicEditor}
          data={getValues(`body${index + 1}`)}
          onChange={(event, editor) => {
            setValue(`body${index + 1}`, editor.getData());
            trigger(`body${index + 1}`);
          }}
        />
      </Box>
      <Box sx={{ mt: 3 }}>
        <Typography>Visible</Typography>
        <Switch
          color="secondary"
          checked={checkedBlock[index]}
          name={`${index}`}
          onChange={handleChangeBlock}
          inputProps={{ 'aria-label': 'controlled' }}
        />
      </Box>
      <Box sx={{ mt: 3 }}>
        <Typography>
          Media *Optional* (16:9, 1920 x 1080 px, JPG/PNG max. 5MB or MP4 max.
          2GB)
        </Typography>
        <Grid
          container
          direction="row"
          justifyContent="center"
          alignItems="stretch"
          spacing={3}
        >
          <Grid item xs={12}>
            {myFiles.length + getMyFiles?.length < 3 && (
              <div {...getRootProps({ className: 'dropzone' })}>
                <input {...getInputProps()} />
                <DropzoneBox>
                  <Box textAlign={'center'}>
                    <CloudUploadOutlinedIcon fontSize="large" />
                    <Typography
                      fontSize={20}
                      sx={{
                        '& span': {
                          textDecoration: 'underline'
                        }
                      }}
                    >
                      Drag 'n' drop some files here, or{' '}
                      <span>click to select files</span>
                    </Typography>
                  </Box>
                </DropzoneBox>
              </div>
            )}
            <Box mt={3}>{files.length > 0 && files}</Box>
            <Box mt={3}>{getMyFiles?.length > 0 && showOldFile}</Box>
            <Box>{fileRejectionItems}</Box>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
}

export default ContentBlock;
