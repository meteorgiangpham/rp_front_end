import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { CKEditor } from '@ckeditor/ckeditor5-react';
import CloseIcon from '@mui/icons-material/Close';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import {
  Box,
  Button,
  DialogContent,
  Snackbar,
  Switch,
  TextField,
  Typography
} from '@mui/material';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import PropTypes from 'prop-types';
import { useContext, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import page from 'src/api/page';
import { AuthContext } from 'src/App';

function SimpleDialog(props) {
  const { onClose, open, id, handleOpenToast, changeToastMessage } = props;
  const { updateSuccess } = useContext(AuthContext);
  const [checked, setChecked] = useState<boolean>(true);
  const [checkedBlock, setCheckedBlock] = useState<Array<boolean>>([
    true,
    true,
    true
  ]);

  const [allMedia, setAllMedia] = useState({
    backgroundImageUrl: ''
  });

  const [allMediaContentBlock, setAllMediaContentBlock] = useState<any>([
    '',
    '',
    ''
  ]);
  const [allMediaThumbnailContentBlock, setAllMediaThumbnailContentBlock] =
    useState<any>(['', '', '']);

  const [contentBlock, setContentBlock] = useState<number>(0);

  const { register, getValues, handleSubmit, setValue, trigger, watch } =
    useForm({
      mode: 'onChange'
    });
  const onSubmit = async (data) => {
    const {
      title,
      subtitle,
      background,
      description,
      visible,
      body1,
      body2,
      body3,
      mediaContent1,
      mediaContent2,
      mediaContent3,
      mediaContentThumbnail1,
      mediaContentThumbnail2,
      mediaContentThumbnail3,
      idBlock1,
      idBlock2,
      idBlock3,
      headerContentBlock1,
      headerContentBlock2,
      headerContentBlock3,
      visible1,
      visible2,
      visible3
    } = data;

    const formData = new FormData();
    formData.append('Id', id);
    formData.append('Title', title);
    formData.append('SubTitle', subtitle);
    formData.append('Description', description);
    formData.append('IsVisible', visible);

    const lstContentBlocks = [
      {
        header: headerContentBlock1,
        description: body1,
        isShowMedia: visible1,
        thumbnail: mediaContentThumbnail1 ? mediaContentThumbnail1[0] : null,
        media: mediaContent1 ? mediaContent1[0] : null,
        id: idBlock1
      },
      {
        header: headerContentBlock2,
        description: body2,
        thumbnail: mediaContentThumbnail2 ? mediaContentThumbnail2[0] : null,
        isShowMedia: visible2,
        media: mediaContent2 ? mediaContent2[0] : null,
        id: idBlock2
      },
      {
        header: headerContentBlock3,
        description: body3,
        isShowMedia: visible3,
        thumbnail: mediaContentThumbnail3 ? mediaContentThumbnail3[0] : null,
        media: mediaContent3 ? mediaContent3[0] : null,
        id: idBlock3
      }
    ];

    lstContentBlocks.forEach((item, index) => {
      if (index <= contentBlock - 1) {
        formData.append(`ContentBlocks[${index}].Media`, item.media);
        formData.append(`ContentBlocks[${index}].Thumbnail`, item.thumbnail);
        formData.append(`ContentBlocks[${index}].Header`, item.header);
        formData.append(
          `ContentBlocks[${index}].Description`,
          item.description
        );
        if (item.id) {
          formData.append(`ContentBlocks[${index}].Id`, item.id);
        }

        if (item.isShowMedia) {
          formData.append(
            `ContentBlocks[${index}].IsShowMedia`,
            item.isShowMedia
          );
        }
      }
    });

    if (background[0]) {
      formData.append('Background', background[0]);
    }

    try {
      await page.editPage(formData).then((res) => {
        if (res.data.success) {
          onClose();
          handleOpenToast();
          changeToastMessage(res.data.message);
          updateSuccess();
        } else {
          changeToastMessage('Success');

          handleOpenToast();
        }
      });
    } catch (error) {}
  };
  useEffect(() => {
    if (open) {
      page.getPageById(id).then((res) => {
        if (res.data.success) {
          const data = res.data.data;

          setValue('title', data.title ? data.title : '');
          setValue('subtitle', data.subTitle ? data.subTitle : '');
          setValue('description', data.description ? data.description : '');

          if (data.contentBlocks && data.contentBlocks.length > 0) {
            let tempMedia = [];
            let tempMediaThumbnail = [];

            data.contentBlocks.forEach((d, i) => {
              setValue(`idBlock${i + 1}`, d.id ? d.id : '');
              setValue(`body${i + 1}`, d.description ? d.description : '');
              setValue(`visible${i + 1}`, d.isShowMedia);
              setValue(`headerContentBlock${i + 1}`, d.header ? d.header : '');
              setValue(`mediaContent${i + 1}`, undefined);
              setValue(`mediaContentThumbnail${i + 1}`, undefined);
              tempMedia.push(d.mediaFile);
              tempMediaThumbnail.push(d.thumbnailUrl);
            });
            setAllMediaContentBlock(tempMedia);
            setAllMediaThumbnailContentBlock(tempMediaThumbnail);
          }

          setContentBlock(data.contentBlocks.length);
          handleChangeFormOpen(data.isVisible);

          setAllMedia({
            backgroundImageUrl: data.backgroundImageUrl
              ? data.backgroundImageUrl
              : ''
          });
        }
      });
    }
  }, [id, setValue, open]);

  const handleListFile = (file: File) => {
    if (
      file.type.toLowerCase() === 'image/jpg' ||
      file.type.toLowerCase() === 'image/jpeg' ||
      file.type.toLowerCase() === 'image/png'
    ) {
      if (file.size <= 5012 * 1024) return <p>{file.name}</p>;
      else return <p style={{ color: 'red' }}>This file is too large</p>;
    } else if (file.type.toLowerCase() === 'video/mp4') {
      if (file.size <= 2147483648) return <p>{file.name}</p>;
      else return <p style={{ color: 'red' }}>This file is too large</p>;
    } else {
      return <p style={{ color: 'red' }}>Incorrect format</p>;
    }
  };
  const handleUploadFile = (file: File) => {
    if (
      file.type.toLowerCase() === 'image/jpg' ||
      file.type.toLowerCase() === 'image/jpeg' ||
      file.type.toLowerCase() === 'image/png'
    ) {
      if (file.size <= 5012 * 1024) {
        return <p>{file.name}</p>;
      } else return <p style={{ color: 'red' }}>This file is too large</p>;
    } else return <p style={{ color: 'red' }}>Incorrect format</p>;
  };
  useEffect(() => {
    register('body1');
    register('body2');
    register('body3');
    register('visible1');
    register('visible2');
    register('visible3');

    register('idBlock1');
    register('idBlock2');
    register('idBlock3');
  });
  const watchVideo = watch(['background']);
  const watchMediaContent = watch([
    'mediaContent1',
    'mediaContent2',
    'mediaContent3'
  ]);

  const watchThumbail = watch([
    'mediaContentThumbnail1',
    'mediaContentThumbnail2',
    'mediaContentThumbnail3'
  ]);

  const handleChange = (event) => {
    setChecked(event.target.checked);
    setValue('visible', event.target.checked);
  };
  const handleChangeBlock = (event) => {
    let index = +event.target.name;
    let temp = checkedBlock;
    temp[index] = Boolean(event.target.checked);

    setCheckedBlock({ ...checkedBlock, ...temp });
    setValue(`visible${index + 1}`, event.target.checked);
  };
  const handleChangeFormOpen = (check: boolean) => {
    setChecked(check);
    setValue('visible', check);
  };

  return (
    <Dialog maxWidth="md" fullWidth={true} onClose={onClose} open={open}>
      <DialogTitle>Edit Section/Page</DialogTitle>
      <Dialog maxWidth="md" fullWidth={true} onClose={onClose} open={open}>
        <DialogTitle>
          {' '}
          <Typography
            variant="h3"
            sx={{ fontWeight: 'bold', textTransform: 'uppercase' }}
          >
            View Detail Section/Page
          </Typography>
        </DialogTitle>
        <DialogContent sx={{ width: { md: '100%', xs: '100%', sm: '100%' } }}>
          <Box component="form" onSubmit={handleSubmit(onSubmit)}>
            <Box sx={{ mt: 3 }}>
              <TextField
                required
                disabled={true}
                id="outlined-basic"
                label="Section header"
                variant="outlined"
                {...register('title', { required: true, maxLength: 70 })}
                sx={{ width: '100%' }}
              />
            </Box>
            <Box sx={{ mt: 3 }}>
              <TextField
                id="outlined-basic"
                label="Page title"
                disabled={true}
                variant="outlined"
                {...register('subtitle')}
                sx={{ width: '100%' }}
              />
            </Box>
            <Box sx={{ mt: 3 }}>
              <Typography>
                Page body text <span style={{ color: 'red' }}>*Optional*</span>{' '}
              </Typography>
              <CKEditor
                editor={ClassicEditor}
                disabled={true}
                data={getValues(`description`)}
                onChange={(event, editor) => {
                  setValue('description', editor.getData());
                  trigger('description');
                }}
              />
            </Box>
            <Box sx={{ mt: 3 }}>
              {contentBlock >= 1 &&
                [...Array(contentBlock)].map((i, index) => (
                  <Box
                    key={index}
                    sx={{ mt: 3, border: '2px solid #000', p: 3 }}
                  >
                    <Box
                      sx={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'space-between'
                      }}
                    >
                      <Typography>Content block {index + 1}</Typography>
                      <Button
                        disabled={true}
                        sx={{
                          display: `${
                            contentBlock !== index + 1 ? 'none' : 'inherit'
                          }`,
                          padding: 0,
                          minWidth: 'auto'
                        }}
                        onClick={() => {
                          setContentBlock((prev) => prev - 1);
                          setValue(`body${index + 1}`, undefined);
                        }}
                      >
                        <CloseIcon />
                      </Button>
                    </Box>
                    <Box sx={{ mt: 3 }}>
                      <TextField
                        label={`Title`}
                        variant="outlined"
                        disabled={true}
                        {...register(`headerContentBlock${index + 1}`)}
                        sx={{ width: '100%' }}
                      />
                    </Box>
                    <Box sx={{ mt: 3 }}>
                      <Typography>Body text</Typography>
                      <CKEditor
                        editor={ClassicEditor}
                        disabled={true}
                        data={getValues(`body${index + 1}`)}
                        onChange={(event, editor) => {
                          setValue(`body${index + 1}`, editor.getData());
                          trigger(`body${index + 1}`);
                        }}
                      />
                    </Box>
                    <Box sx={{ mt: 3 }}>
                      <Typography>Visible</Typography>
                      <Switch
                        checked={checkedBlock[index]}
                        name={`${index}`}
                        disabled={true}
                        color="secondary"
                        onChange={handleChangeBlock}
                        inputProps={{ 'aria-label': 'controlled' }}
                      />
                    </Box>
                    <Box sx={{ mt: 3 }}>
                      <Typography>
                        Media *Optional* (16:9, 1920 x 1080 px, JPG/PNG max. 5MB
                        or MP4 max. 2GB)
                      </Typography>

                      {watchMediaContent[index] && (
                        <Typography sx={{ fontStyle: 'italic' }}>
                          {Object.keys(watchMediaContent[index]).map(
                            (file, i) => (
                              <Typography key={i}>
                                {handleListFile(watchMediaContent[index][i])}
                              </Typography>
                            )
                          )}
                        </Typography>
                      )}

                      {(!watchMediaContent[index] ||
                        !watchMediaContent[index][0]) &&
                        allMediaContentBlock[index] && (
                          <Typography>
                            {allMediaContentBlock[index].includes('mp4') ? (
                              <video
                                src={allMediaContentBlock[index]}
                                width="200"
                                height="125"
                                controls
                              ></video>
                            ) : (
                              <img
                                src={allMediaContentBlock[index]}
                                alt="led"
                                width="200"
                                height="125"
                              />
                            )}
                          </Typography>
                        )}
                    </Box>
                    {((watchMediaContent[index] &&
                      watchMediaContent[index][0] &&
                      watchMediaContent[index][0].type === 'video/mp4') ||
                      (allMediaContentBlock[index] &&
                        allMediaContentBlock[index].includes('mp4'))) && (
                      <Box sx={{ mt: 3 }}>
                        <Typography>
                          Thumbnail image (16:9, 1920 x 1080 px, JPG/PNG max
                          5MB)
                        </Typography>

                        {watchThumbail[index] && watchThumbail[index][0] && (
                          <Typography sx={{ fontStyle: 'italic' }}>
                            {handleUploadFile(watchThumbail[index][0])}
                          </Typography>
                        )}

                        {(!watchThumbail[index] ||
                          !watchThumbail[index][0] ||
                          allMediaThumbnailContentBlock[index]) && (
                          <Typography>
                            <img
                              src={allMediaThumbnailContentBlock[index]}
                              alt="thumbnail"
                              width="200"
                              height="125"
                            />
                          </Typography>
                        )}
                      </Box>
                    )}
                  </Box>
                ))}
            </Box>
            <Box sx={{ mt: 3 }}>
              <Typography>Background</Typography>

              {watchVideo[0] && watchVideo[0][0] && (
                <Typography sx={{ fontStyle: 'italic' }}>
                  {handleUploadFile(watchVideo[0][0])}
                </Typography>
              )}
              {(!watchVideo[0] || !watchVideo[0][0]) &&
                allMedia.backgroundImageUrl && (
                  <Typography>{allMedia.backgroundImageUrl}</Typography>
                )}
            </Box>
            <Box sx={{ mt: 3 }}>
              <Typography>Visible</Typography>
              <Switch
                checked={checked}
                color="secondary"
                onChange={handleChange}
                disabled={true}
                inputProps={{ 'aria-label': 'controlled' }}
              />
            </Box>
          </Box>
        </DialogContent>
      </Dialog>
    </Dialog>
  );
}

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  id: PropTypes.string.isRequired,
  handleOpenToast: PropTypes.func.isRequired,
  changeToastMessage: PropTypes.func.isRequired
};

interface PropsId {
  id: string;
}

function Edit({ id }: PropsId) {
  const [open, setOpen] = useState(false);
  const [openToast, setOpenToast] = useState(false);
  const [msg, setMsg] = useState<string>('');
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleCloseToast = () => {
    setOpenToast(false);
  };

  const handleOpenToast = () => {
    setOpenToast(true);
  };

  const changeToastMessage = (msg: string) => {
    setMsg(msg);
  };
  return (
    <>
      <RemoveRedEyeIcon fontSize="small" onClick={handleClickOpen} />

      <SimpleDialog
        open={open}
        onClose={handleClose}
        id={id}
        changeToastMessage={changeToastMessage}
        handleOpenToast={handleOpenToast}
      />
      <Snackbar
        autoHideDuration={3000}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={openToast}
        onClose={handleCloseToast}
        message={msg}
        key={'top' + 'right'}
      />
    </>
  );
}

export default Edit;
