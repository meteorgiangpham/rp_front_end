import PropTypes from 'prop-types';
import { useEffect, useState } from 'react';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import { Box, DialogContent, Typography } from '@mui/material';
import { useForm } from 'react-hook-form';
import CloseIcon from '@mui/icons-material/Close';
const emails = ['username@gmail.com', 'user02@gmail.com'];

function SimpleDialog(props) {
  const { onClose, open } = props;

  return (
    <Dialog maxWidth="xl" fullWidth={true} onClose={onClose} open={open}>
      <DialogTitle>View detail</DialogTitle>
      <Dialog maxWidth="xl" fullWidth={true} onClose={onClose} open={open}>
        <DialogTitle
          sx={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between'
          }}
        >
          {' '}
          <Typography
            variant="h3"
            sx={{ fontWeight: 'bold', textTransform: 'uppercase' }}
          >
            View detail
          </Typography>
          <CloseIcon onClick={onClose} />
        </DialogTitle>
        <DialogContent sx={{ width: { md: '100%', xs: '100%', sm: '100%' } }}>
          <Box>
            <Typography>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ea,
                labore quas ad harum praesentium a iure ducimus eligendi nemo
                asperiores qui, iste error impedit beatae pariatur quae
                officiis, optio eum. Lorem ipsum dolor sit amet consectetur
                adipisicing elit. Hic sequi ratione qui, asperiores quis magni
                fuga facilis expedita praesentium velit rem odio adipisci
                necessitatibus pariatur dolores tempora sapiente recusandae
                quidem? Repellat, cum quos?
              </p>
              <p>
                Quo possimus quaerat corrupti nihil quasi, quibusdam, dicta
                quisquam perspiciatis labore provident repellat fuga architecto
                facere iure voluptate inventore eos tempore asperiores maiores
                amet quos explicabo! Possimus? Quasi nemo aperiam et quos ipsam
                exercitationem, doloribus culpa harum necessitatibus libero
                saepe illo, iste perspiciatis aliquam reprehenderit impedit
                suscipit nesciunt architecto excepturi accusantium! Nihil optio
                libero molestiae consequuntur fuga? Consequuntur nisi
                repudiandae consectetur odit facilis unde autem sunt id
                possimus, ab ratione. Suscipit laboriosam laudantium asperiores
                omnis reiciendis minima quaerat tempore reprehenderit,
                dignissimos laborum non itaque vero illum nihil. Quidem deserunt
                illo animi ducimus? Veniam, dignissimos incidunt nostrum numquam
                eligendi dolor sunt id assumenda! Nisi, reprehenderit libero
                iusto, veniam impedit animi eius, aperiam velit unde consequatur
                similique saepe itaque.
              </p>
            </Typography>
          </Box>
        </DialogContent>
      </Dialog>
    </Dialog>
  );
}

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired
};

function ViewDetail() {
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <RemoveRedEyeIcon fontSize="small" onClick={handleClickOpen} />

      <SimpleDialog open={open} onClose={handleClose} />
    </>
  );
}

export default ViewDetail;
