import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import AddTwoToneIcon from '@mui/icons-material/AddTwoTone';

import {
  Box,
  DialogContent,
  MenuItem,
  Select,
  TextField,
  Typography
} from '@mui/material';
import { useForm } from 'react-hook-form';

function SimpleDialog(props) {
  const { onClose, open } = props;

  // React-hook-form
  const { register, handleSubmit, setValue, trigger } = useForm({
    mode: 'onChange'
  });
  const onSubmit = (data) => {};
  const [role, setRole] = useState<string>('Admin');

  useEffect(() => {
    register('role');
    setValue('role', 'Admin');
  });

  const handleChangeRole = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRole(event.target.value);
    setValue('role', event.target.value);
    trigger('role');
  };

  return (
    <Dialog maxWidth="md" fullWidth={true} onClose={onClose} open={open}>
      <DialogTitle>
        {' '}
        <Typography
          variant="h3"
          sx={{ fontWeight: 'bold', textTransform: 'uppercase' }}
        >
          Add User
        </Typography>
      </DialogTitle>
      <DialogContent sx={{ width: '100%' }}>
        <Box component="form" onSubmit={handleSubmit(onSubmit)}>
          <Box sx={{ mt: 3 }}>
            <TextField
              required
              id="outlined-basic"
              label="Firstname"
              variant="outlined"
              {...register('firstname', { required: true, maxLength: 70 })}
              sx={{ width: '100%' }}
            />
          </Box>
          <Box sx={{ mt: 3 }}>
            <TextField
              required
              id="outlined-basic"
              label="Lastname"
              variant="outlined"
              {...register('lastname', { required: true, maxLength: 70 })}
              sx={{ width: '100%' }}
            />
          </Box>
          <Box sx={{ mt: 3 }}>
            <TextField
              required
              type="email"
              id="outlined-basic"
              label="Email"
              variant="outlined"
              {...register('email', { required: true, maxLength: 70 })}
              sx={{ width: '100%' }}
            />
          </Box>
          <Box sx={{ mt: 3 }}>
            <TextField
              required
              type="password"
              id="outlined-basic"
              label="Password"
              variant="outlined"
              {...register('password', { required: true, maxLength: 70 })}
              sx={{ width: '100%' }}
            />
          </Box>
          <Box sx={{ mt: 3 }}>
            <TextField
              required
              type="password"
              id="outlined-basic"
              label="Confirm Password"
              variant="outlined"
              {...register('confirmPassword', {
                required: true,
                maxLength: 70
              })}
              sx={{ width: '100%' }}
            />
          </Box>

          <Box sx={{ mt: 3 }}>
            <Typography>Select role</Typography>
            <Select
              value={role}
              onChange={handleChangeRole}
              displayEmpty
              inputProps={{ 'aria-label': 'Without label' }}
            >
              <MenuItem value="Admin">Admin</MenuItem>
              <MenuItem value="User">User</MenuItem>
            </Select>
          </Box>

          <Box sx={{ mt: 3 }}>
            <Button
              color="secondary"
              type="submit"
              variant="contained"
              sx={{ width: '100%' }}
            >
              Add user
            </Button>
          </Box>
        </Box>
      </DialogContent>
    </Dialog>
  );
}

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired
};

function Add() {
  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <Button
        onClick={handleClickOpen}
        sx={{ mt: { xs: 2, md: 0 } }}
        variant="contained"
        startIcon={<AddTwoToneIcon fontSize="small" />}
      >
        Create
      </Button>
      <SimpleDialog open={open} onClose={handleClose} />
    </>
  );
}

export default Add;
