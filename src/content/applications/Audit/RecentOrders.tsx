import { Card } from '@mui/material';

import RecentOrdersTable from './RecentOrdersTable';

import { Audit } from 'src/models';
import { useEffect, useState } from 'react';
import loginApi from 'src/api/loginApi';

function RecentOrders() {
  useEffect(() => {
    loginApi.getAudit().then((res) => {
      if (res.data.success) {
        setCryptoOrders(res.data.data);
      }
    });
  }, []);
  const [cryptoOrders, setCryptoOrders] = useState<Audit[]>([]);
  // const cryptoOrders: Audit[] = [
  //   {
  //     id: '1',
  //     name: 'User InActive  1',
  //     email: 'User@gmail.com',
  //     date: new Date(),
  //     action: 'login'
  //   },
  //   {
  //     id: '2',
  //     name: 'User 2',
  //     email: 'User@gmail.com',
  //     date: new Date(),
  //     action: 'logout'
  //   },
  //   {
  //     id: '3',
  //     name: 'User 3',
  //     email: 'User@gmail.com',
  //     date: new Date(),
  //     action: 'login'
  //   },
  //   {
  //     id: '4',
  //     name: 'User 4',
  //     email: 'User@gmail.com',
  //     date: new Date(),
  //     action: 'logout'
  //   },
  //   {
  //     id: '5',
  //     name: 'User 5',
  //     email: 'User@gmail.com',
  //     date: new Date(),
  //     action: 'logout'
  //   },
  //   {
  //     id: '6',
  //     name: 'User 6',
  //     email: 'User@gmail.com',
  //     date: new Date(),
  //     action: 'login'
  //   },
  //   {
  //     id: '7',
  //     name: 'User 7',
  //     email: 'User@gmail.com',
  //     date: new Date(),
  //     action: 'login'
  //   },
  //   {
  //     id: '8',
  //     name: 'User 8',
  //     email: 'User@gmail.com',
  //     date: new Date(),
  //     action: 'login'
  //   },
  //   {
  //     id: '9',
  //     name: 'User 9',
  //     email: 'User@gmail.com',
  //     date: new Date(),
  //     action: 'login'
  //   },
  //   {
  //     id: '10',
  //     name: 'User 10',
  //     email: 'User@gmail.com',
  //     date: new Date(),
  //     action: 'login'
  //   }
  // ];

  return (
    <Card>
      <RecentOrdersTable cryptoOrders={cryptoOrders} />
    </Card>
  );
}

export default RecentOrders;
