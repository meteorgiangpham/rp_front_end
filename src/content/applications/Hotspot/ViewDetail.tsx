import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import {
  Box,
  DialogContent,
  Grid,
  Snackbar,
  Switch,
  TextField,
  Typography
} from '@mui/material';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import PropTypes from 'prop-types';
import { useContext, useEffect, useRef, useState } from 'react';
import { useForm } from 'react-hook-form';
import hotspot from 'src/api/hotspot';
import { AuthContext } from 'src/App';
import Canvas from 'src/components/Common/Canvas';
import Jodit from 'src/components/TinyEditor/JoditEditor';

function SimpleDialog(props) {
  const { onClose, open, id, disable, handleOpenToast, changeToastMessage } =
    props;

  const { updateSuccess } = useContext(AuthContext);

  const [checked, setChecked] = useState<boolean>(true);

  const [allMedia, setAllMedia] = useState({
    hightlightMediaUrl: '',
    mediaUrl: '',
    thumbnailImageUrl: '',
    hotspotImage: ''
  });

  const { register, handleSubmit, setValue, trigger, watch, getValues } =
    useForm({
      mode: 'onChange'
    });
  const onSubmit = async (data) => {
    const {
      title,
      body,
      media,
      video,
      visible,
      mediaContentThumbnail,
      coordinatesX,
      coordinatesY
    } = data;

    const formData = new FormData();
    formData.append('Id', id);
    formData.append('Title', title);
    formData.append('Body', body);

    formData.append('CoordinateX', coordinatesX);
    formData.append('CoordinateY', coordinatesY);
    formData.append('IsVisible', visible);

    if (video !== undefined && video[0] !== undefined) {
      formData.append('HighLightMedia', video[0]);
    }

    if (media !== undefined && media[0] !== undefined) {
      formData.append('Media', media[0]);
    }
    if (
      mediaContentThumbnail !== undefined &&
      mediaContentThumbnail[0] !== undefined
    ) {
      formData.append('Thumbnail', mediaContentThumbnail[0]);
    }

    try {
      await hotspot.editHotspot(formData).then((res) => {
        if (res.data.success) {
          onClose();
          handleOpenToast();
          changeToastMessage(res.data.message);
          updateSuccess();
        } else {
          changeToastMessage('Success');

          handleOpenToast();
        }
      });
    } catch (error) {}
  };

  useEffect(() => {
    if (open) {
      hotspot.getHotspotById(id).then((res) => {
        if (res.data.success) {
          const data = res.data.data;

          setValue('title', data.title);
          setValue('body', data.body);
          setValue('coordinatesX', +data.coordinateX);
          setValue('coordinatesY', +data.coordinateY);
          handleChangeFormOpen(data.isVisible);
          // setValue('visible', data.isVisible);

          setAllMedia({
            hightlightMediaUrl: data.highlightMediaUrl,
            mediaUrl: data.mediaUrl,
            thumbnailImageUrl: data.thumbnailUrl,
            hotspotImage: data.hostpotImageUrl
          });
        }
      });
    }
  }, [id, setValue, open]);

  const handleListFile = (file: File) => {
    if (file) {
      if (
        file.type.toLowerCase() === 'image/jpg' ||
        file.type.toLowerCase() === 'image/jpeg' ||
        file.type.toLowerCase() === 'image/png'
      ) {
        if (file.size <= 5012 * 1024) return <p>{file.name}</p>;
        else return <p style={{ color: 'red' }}>This file is too large</p>;
      } else if (file.type.toLowerCase() === 'video/mp4') {
        if (file.size <= 2147483648) return <p>{file.name}</p>;
        else return <p style={{ color: 'red' }}>This file is too large</p>;
      } else {
        return <p style={{ color: 'red' }}>Incorrect format</p>;
      }
    }
  };
  useEffect(() => {
    register('body');
    register('visible');
    register('mediaContentThumbnail');
  });

  const watchVideo = watch(['video', 'media']);
  const watchVideoThumbnail = watch(['mediaContentThumbnail']);
  const handleChange = (event) => {
    setChecked(event.target.checked);
    setValue('visible', event.target.checked);
  };
  const handleChangeFormOpen = (check: boolean) => {
    setChecked(check);
    setValue('visible', check);
  };

  const handleGetDataFromEditor = (data: string) => {
    setValue('body', data);
    trigger('body');
  };
  const coorX = useRef({});
  coorX.current = watch('coordinatesX', '');
  const coorY = useRef({});
  coorY.current = watch('coordinatesY', '');

  return (
    <Dialog maxWidth="md" fullWidth={true} onClose={onClose} open={open}>
      <DialogTitle>Edit</DialogTitle>
      <Dialog maxWidth="md" fullWidth={true} onClose={onClose} open={open}>
        <DialogTitle>
          {' '}
          <Typography
            variant="h3"
            sx={{ fontWeight: 'bold', textTransform: 'uppercase' }}
          >
            View Detail Touchscreen Hotspot
          </Typography>
        </DialogTitle>
        <DialogContent sx={{ width: { md: '100%', xs: '100%', sm: '100%' } }}>
          <Box component="form" onSubmit={handleSubmit(onSubmit)}>
            <Box sx={{ mt: 3 }}>
              <TextField
                required
                defaultValue="Hello World"
                // required
                id="outlined-basic"
                label="Title (max. 80 characters)"
                variant="outlined"
                disabled={disable}
                {...register('title', { required: true, maxLength: 70 })}
                sx={{ width: '100%' }}
              />
            </Box>
            <Box sx={{ mt: 3 }}>
              <Typography>Body text (max. 1000 characters)</Typography>
              <Jodit
                initialValue={getValues('body') as string}
                limit={1000}
                disable={disable}
                handleGetDataFromEditor={handleGetDataFromEditor}
              />
            </Box>
            <Box sx={{ mt: 3 }}>
              <Typography>Visible</Typography>
              <Switch
                checked={checked}
                disabled={disable}
                color="secondary"
                onChange={handleChange}
                inputProps={{ 'aria-label': 'controlled' }}
              />
            </Box>
            <Grid
              container
              rowSpacing={1}
              columnSpacing={{ xs: 1, sm: 1, md: 2 }}
              sx={{ mt: 3 }}
            >
              <Grid item xs={6}>
                <TextField
                  id="outlined-basic"
                  label="Coordinates X"
                  variant="outlined"
                  defaultValue="1"
                  type="number"
                  {...register('coordinatesX')}
                  sx={{ width: '100%' }}
                  disabled={disable}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  id="outlined-basic"
                  label="Coordinates y"
                  variant="outlined"
                  defaultValue="1"
                  type="number"
                  {...register('coordinatesY')}
                  sx={{ width: '100%' }}
                  disabled={disable}
                />
              </Grid>
            </Grid>
            <Box sx={{ mt: 3 }}>
              <Canvas
                x={coorX.current}
                y={coorY.current}
                image={allMedia.hotspotImage}
              />
            </Box>

            <Box sx={{ mt: 3 }}>
              <Typography>Highlight Media URL</Typography>

              {watchVideo[0] && watchVideo[0][0] && (
                <Typography sx={{ fontStyle: 'italic' }}>
                  {handleListFile(watchVideo[0][0])}
                </Typography>
              )}

              {(!watchVideo[0] || !watchVideo[0][0]) &&
                allMedia.hightlightMediaUrl && (
                  <Typography sx={{ mt: 3 }}>
                    <img
                      src={allMedia.hightlightMediaUrl}
                      alt="led"
                      width="200"
                      height="125"
                    />
                  </Typography>
                )}
            </Box>

            <Box sx={{ mt: 3 }}>
              <Typography>
                Touchscreen Screensaver{' '}
                <span style={{ color: 'red' }}>*Optional*</span> (16:9, 1920 x
                1080 px, JPG/PNG max. 5MB or MP4 max. 2GB)
              </Typography>

              {watchVideo[1] && watchVideo[1][0] && (
                <Typography sx={{ fontStyle: 'italic' }}>
                  {handleListFile(watchVideo[1][0])}
                </Typography>
              )}
              {(!watchVideo[1] || !watchVideo[1][0]) && allMedia.mediaUrl && (
                <Typography sx={{ mt: 3 }}>
                  {allMedia.mediaUrl.includes('mp4') ? (
                    <video
                      src={allMedia.mediaUrl}
                      width="200"
                      height="125"
                      controls
                    ></video>
                  ) : (
                    <img
                      src={allMedia.mediaUrl}
                      alt="led"
                      width="200"
                      height="125"
                    />
                  )}
                </Typography>
              )}
            </Box>
            {((watchVideo[1] &&
              watchVideo[1][0] &&
              watchVideo[1][0].type === 'video/mp4') ||
              (allMedia.thumbnailImageUrl &&
                allMedia.mediaUrl.includes('mp4'))) && (
              <Box sx={{ mt: 3 }}>
                <Typography>
                  Thumbnail image (16:9, 1920 x 1080 px, JPG/PNG max 5MB)
                </Typography>

                {watchVideoThumbnail[0] && watchVideoThumbnail[0][0] && (
                  <Typography sx={{ fontStyle: 'italic' }}>
                    {handleListFile(watchVideoThumbnail[0][0])}
                  </Typography>
                )}

                {(!watchVideoThumbnail[0] || !watchVideoThumbnail[0][0]) &&
                  allMedia.thumbnailImageUrl && (
                    <Typography sx={{ mt: 3 }}>
                      <img
                        src={allMedia.thumbnailImageUrl}
                        alt="led"
                        width="200"
                        height="125"
                      />
                    </Typography>
                  )}
              </Box>
            )}
          </Box>
        </DialogContent>
      </Dialog>
    </Dialog>
  );
}

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  id: PropTypes.string.isRequired,
  disable: PropTypes.bool.isRequired,
  handleOpenToast: PropTypes.func.isRequired,
  changeToastMessage: PropTypes.func.isRequired
};
interface PropsId {
  id: string;
}
function ViewDetail({ id }: PropsId) {
  const [open, setOpen] = useState(false);
  const [openToast, setOpenToast] = useState(false);
  const [msg, setMsg] = useState<string>('');
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleCloseToast = () => {
    setOpenToast(false);
  };

  const handleOpenToast = () => {
    setOpenToast(true);
  };

  const changeToastMessage = (msg: string) => {
    setMsg(msg);
  };
  return (
    <>
      <RemoveRedEyeIcon fontSize="small" onClick={handleClickOpen} />

      <SimpleDialog
        open={open}
        onClose={handleClose}
        id={id}
        changeToastMessage={changeToastMessage}
        handleOpenToast={handleOpenToast}
        disable={true}
      />
      <Snackbar
        autoHideDuration={3000}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={openToast}
        onClose={handleCloseToast}
        message={msg}
        key={'top right'}
      />
    </>
  );
}

export default ViewDetail;
