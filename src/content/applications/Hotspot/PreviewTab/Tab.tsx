import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import Typography from '@mui/material/Typography';
import * as React from 'react';
import hostpot from 'src/api/hotspot';
import CanvasPreview from 'src/components/Common/CanvasPreview';

interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`
  };
}

export default function BasicTabs() {
  const [value, setValue] = React.useState(0);
  const [imageList, setImageList] = React.useState([]);
  const [imageBackgroundList, setImageBackgroundList] = React.useState([]);

  const [coor, setCoor] = React.useState([
    {
      coorX: 0,
      coorY: 0
    },
    {
      coorX: 0,
      coorY: 0
    },
    {
      coorX: 0,
      coorY: 0
    },
    {
      coorX: 0,
      coorY: 0
    },
    {
      coorX: 0,
      coorY: 0
    }
  ]);
  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  React.useEffect(() => {
    hostpot.getHotspot().then((res) => {
      if (res.data.success) {
        let data = res.data.data;
        let images = data.map((d) => d.hostpotImageUrl);
        let imagesBG = data.map((d) => d.backgroundHostpotImage);

        setImageList(images);
        setImageBackgroundList(imagesBG);
        let coorTemp = [];
        data.map((d) =>
          coorTemp.push({ coorX: d.coordinateX, coorY: d.coordinateY })
        );
        setCoor(coorTemp);
      }
    });
  }, []);

  return (
    <Box sx={{ width: '100%' }}>
      <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="basic tabs example"
        >
          <Tab label="Hotspot 1" {...a11yProps(0)} />
          <Tab label="Hotspot 2" {...a11yProps(1)} />
          <Tab label="Hotspot 3" {...a11yProps(2)} />
          <Tab label="Hotspot 4" {...a11yProps(3)} />
          <Tab label="Hotspot 5" {...a11yProps(4)} />
        </Tabs>
      </Box>
      {[...Array(5)].map((d, index) => (
        <TabPanel value={value} key={index} index={index}>
          <CanvasPreview
            x={coor[index].coorX ? coor[index].coorX : 0}
            y={coor[index].coorY ? coor[index].coorY : 0}
            imageBackgroundList={imageBackgroundList[index]}
            image={imageList[index]}
          />
        </TabPanel>
      ))}
    </Box>
  );
}
