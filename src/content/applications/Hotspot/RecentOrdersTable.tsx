import {
  Box,
  Button,
  Card,
  CardContent,
  CircularProgress,
  Divider,
  FormControl,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
  Snackbar,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  Tooltip,
  Typography,
  useTheme
} from '@mui/material';
import PropTypes from 'prop-types';
import { ChangeEvent, FC, useContext, useState } from 'react';
import { useForm } from 'react-hook-form';
import hostpot from 'src/api/hotspot';
import { AuthContext } from 'src/App';
import Label from 'src/components/Label';
import { Hotspot } from 'src/models/';
import { CryptoOrderStatus } from 'src/models/crypto_order';
import Edit from './Edit';
import ViewDetail from './ViewDetail';

interface RecentOrdersTableProps {
  className?: string;
  cryptoOrders: Hotspot[];
  idStation: string;
  backgroundHostpot: string;
}

interface Filters {
  status?: CryptoOrderStatus;
}

const getStatusLabel = (cryptoOrderStatus: boolean): JSX.Element => {
  const map = {
    true: {
      text: 'Visible',
      color: 'success'
    },
    false: {
      text: 'Hidden',
      color: 'error'
    }
  };

  const { text, color }: any = map[cryptoOrderStatus.toString()];

  return <Label color={color}>{text}</Label>;
};

const applyFilters = (cryptoOrders: Hotspot[], filters: Filters): Hotspot[] => {
  return cryptoOrders.filter((cryptoOrder) => {
    let matches = true;
    if (filters.status && cryptoOrder.isVisible.toString() !== filters.status) {
      matches = false;
    }
    return matches;
  });
};

const applyPagination = (
  cryptoOrders: Hotspot[],
  page: number,
  limit: number
): Hotspot[] => {
  return cryptoOrders.slice(page * limit, page * limit + limit);
};

const RecentOrdersTable: FC<RecentOrdersTableProps> = ({
  cryptoOrders,
  idStation,
  backgroundHostpot
}) => {
  const selectedCryptoOrders = [];
  const selectedBulkActions = selectedCryptoOrders.length > 0;
  const [page, setPage] = useState<number>(0);
  const [limit, setLimit] = useState<number>(10);
  const [filters, setFilters] = useState<Filters>({
    status: null
  });
  const { updateSuccess } = useContext(AuthContext);
  const [loading, setLoading] = useState<boolean>(false);
  const [errorMessage, setErrorMessage] = useState<boolean>(false);
  const [openToast, setOpenToast] = useState<boolean>(false);
  const { register, handleSubmit, watch, setValue } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange'
  });

  const statusOptions = [
    {
      id: 'all',
      name: 'All'
    },
    {
      id: 'true',
      name: 'Visible'
    },
    {
      id: 'false',
      name: 'Hidden'
    }
  ];

  const handleStatusChange = (e: ChangeEvent<HTMLInputElement>): void => {
    let value = null;

    if (e.target.value !== 'all') {
      value = e.target.value;
    }

    setFilters((prevFilters) => ({
      ...prevFilters,
      status: value
    }));
  };

  const handlePageChange = (event: any, newPage: number): void => {
    setPage(newPage);
  };

  const handleLimitChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setLimit(parseInt(event.target.value));
  };

  const filteredCryptoOrders = applyFilters(cryptoOrders, filters);
  const paginatedCryptoOrders = applyPagination(
    filteredCryptoOrders,
    page,
    limit
  );

  const background = watch('background');
  const theme = useTheme();
  const onSubmit = async (data) => {
    setLoading(true);
    const { background } = data;
    if (background !== undefined && background[0] !== undefined) {
      const formData = new FormData();
      formData.append('StationId', idStation);
      formData.append('BackgroundHostpot', background[0]);

      try {
        await hostpot.updateBackgroundHotspot(formData).then((res) => {
          if (res.data.success) {
            setLoading(false);
            setOpenToast(true);
            setErrorMessage(res.data.message);
            updateSuccess();
            setValue('background', undefined);
          } else {
            setErrorMessage(res.data.message);

            setOpenToast(true);
          }
        });
      } catch (error) {
        setLoading(false);
      }
    }
  };
  const handleListFile = (file: File) => {
    if (file) {
      if (
        file.type.toLowerCase() === 'image/jpg' ||
        file.type.toLowerCase() === 'image/jpeg' ||
        file.type.toLowerCase() === 'image/png'
      ) {
        if (file.size <= 5012 * 1024)
          return <p style={{ margin: 0 }}>{file.name}</p>;
        else
          return (
            <p style={{ color: 'red', margin: 0 }}>This file is too large</p>
          );
      } else {
        return <p style={{ color: 'red', margin: 0 }}>Incorrect format</p>;
      }
    }
  };
  return (
    <Card>
      {!selectedBulkActions && (
        <>
          <CardContent sx={{ display: 'flex', alignContent: 'center' }}>
            <Box
              sx={{ width: 'calc(100% - 150px)' }}
              component="form"
              onSubmit={handleSubmit(onSubmit)}
            >
              <Typography fontWeight={'bold'}>
                Background Image Hotspot
              </Typography>
              <FormControl
                fullWidth
                variant="outlined"
                sx={{
                  width: 'calc(100% - 150px)',
                  display: 'flex',
                  flexDirection: 'row',
                  height: '50px',
                  alignItems: 'center'
                }}
              >
                <Box
                  sx={{
                    display: 'flex',
                    alignItems: 'center',
                    flexDirection: 'row',
                    width: 'calc(100% - 150px)'
                  }}
                >
                  <Box>
                    <Box
                      sx={{
                        display: 'flex',
                        alignItems: 'center',
                        flexDirection: 'row',
                        mt: 1
                      }}
                    >
                      <Button
                        variant="contained"
                        component="label"
                        color="primary"
                        sx={{
                          height: '40px',
                          width: '150px',
                          marginRight: '15px'
                        }}
                      >
                        Upload File
                        <input
                          type="file"
                          hidden
                          {...register('background')}
                          accept=".jpg, .png, .jpeg"
                        />
                      </Button>

                      {background !== undefined && background[0] !== undefined
                        ? handleListFile(background[0])
                        : backgroundHostpot &&
                          backgroundHostpot.slice(
                            backgroundHostpot.lastIndexOf('/') + 1
                          )}
                    </Box>
                  </Box>
                </Box>

                {background !== undefined && background[0] !== undefined && (
                  <Button
                    type="submit"
                    color="secondary"
                    variant="contained"
                    sx={{ width: '150px', height: '40px' }}
                  >
                    {loading ? <CircularProgress size={15} /> : 'Submit'}
                  </Button>
                )}
              </FormControl>
            </Box>
            <Box width={150} sx={{ display: 'flex', alignItems: 'center' }}>
              <FormControl fullWidth variant="outlined">
                <InputLabel>Visible</InputLabel>
                <Select
                  value={filters.status || 'all'}
                  onChange={handleStatusChange}
                  label="Status"
                  autoWidth
                >
                  {statusOptions.map((statusOption) => (
                    <MenuItem key={statusOption.id} value={statusOption.id}>
                      {statusOption.name}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>
          </CardContent>
        </>
      )}
      <Divider />
      <TableContainer>
        <Table>
          <TableHead>
            <TableRow
              sx={{
                '& .MuiTableCell-root': {
                  background: '#525252',
                  color: '#fff'
                }
              }}
            >
              <TableCell>Title</TableCell>
              <TableCell>Highlight</TableCell>
              <TableCell>Media</TableCell>
              <TableCell align="right">Coordinates X</TableCell>
              <TableCell align="right">Coordinates Y</TableCell>
              <TableCell align="right">Visible</TableCell>
              <TableCell align="right">Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {paginatedCryptoOrders.map((cryptoOrder) => {
              const isCryptoOrderSelected = selectedCryptoOrders.includes(
                cryptoOrder.id
              );
              return (
                <TableRow
                  hover
                  key={cryptoOrder.id}
                  selected={isCryptoOrderSelected}
                >
                  <TableCell>
                    <Typography
                      variant="body1"
                      fontWeight="bold"
                      color="text.primary"
                      gutterBottom
                      noWrap
                    >
                      {cryptoOrder.title}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography
                      variant="body1"
                      fontWeight="bold"
                      color="text.primary"
                      gutterBottom
                      noWrap
                    >
                      {cryptoOrder.highlightMediaUrl &&
                        cryptoOrder.highlightMediaUrl.slice(
                          cryptoOrder.highlightMediaUrl.lastIndexOf('/') + 1
                        )}
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography
                      variant="body1"
                      fontWeight="bold"
                      color="text.primary"
                      gutterBottom
                      noWrap
                    >
                      {cryptoOrder.mediaUrl &&
                        cryptoOrder.mediaUrl.slice(
                          cryptoOrder.mediaUrl.lastIndexOf('/') + 1
                        )}
                    </Typography>
                  </TableCell>
                  <TableCell align="right">
                    <Typography
                      variant="body1"
                      fontWeight="bold"
                      color="text.primary"
                      gutterBottom
                      noWrap
                    >
                      {cryptoOrder.coordinateX}
                    </Typography>
                  </TableCell>
                  <TableCell align="right">
                    <Typography
                      variant="body1"
                      fontWeight="bold"
                      color="text.primary"
                      gutterBottom
                      noWrap
                    >
                      {cryptoOrder.coordinateY}
                    </Typography>
                  </TableCell>
                  <TableCell align="right">
                    {getStatusLabel(cryptoOrder.isVisible)}
                  </TableCell>
                  <TableCell align="right">
                    <Tooltip title="View Detail" arrow>
                      <IconButton
                        sx={{
                          '&:hover': {
                            background: theme.colors.primary.lighter
                          },
                          color: theme.palette.primary.main
                        }}
                        color="inherit"
                        size="small"
                      >
                        <ViewDetail id={cryptoOrder.id} />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title="Edit" arrow>
                      <IconButton
                        sx={{
                          '&:hover': {
                            background: theme.colors.primary.lighter
                          },
                          color: theme.palette.primary.main
                        }}
                        color="inherit"
                        size="small"
                      >
                        <Edit id={cryptoOrder.id} disable={false} />
                      </IconButton>
                    </Tooltip>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      <Box p={2}>
        <TablePagination
          component="div"
          count={filteredCryptoOrders.length}
          onPageChange={handlePageChange}
          onRowsPerPageChange={handleLimitChange}
          page={page}
          rowsPerPage={limit}
          rowsPerPageOptions={[5, 10, 25, 30]}
        />
      </Box>
      <Snackbar
        autoHideDuration={3000}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={openToast}
        message={errorMessage}
        key={'top right'}
      />
    </Card>
  );
};

RecentOrdersTable.propTypes = {
  cryptoOrders: PropTypes.array.isRequired
};

RecentOrdersTable.defaultProps = {
  cryptoOrders: []
};

export default RecentOrdersTable;
