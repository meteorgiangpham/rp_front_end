import EditTwoToneIcon from '@mui/icons-material/EditTwoTone';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import {
  Box,
  Button,
  CircularProgress,
  DialogContent,
  Grid,
  Snackbar,
  Switch,
  TextField,
  Typography
} from '@mui/material';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import PropTypes from 'prop-types';
import { useContext, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import hotspot from 'src/api/hotspot';
import { AuthContext } from 'src/App';

import CloseIcon from '@mui/icons-material/Close';
import { PreviewImage } from 'src/components/Common/PreviewImage';
import Jodit from 'src/components/TinyEditor/JoditEditor';

function SimpleDialog(props) {
  const { onClose, open, id, disable, handleOpenToast, changeToastMessage } =
    props;

  const { updateSuccess } = useContext(AuthContext);
  const [listDelete, setListDelete] = useState<any>([]);
  const [checked, setChecked] = useState<boolean>(true);
  const [loading, setLoading] = useState<boolean>(false);

  const [allMedia, setAllMedia] = useState({
    highlightMediaUrl: '',
    mediaUrl: '',
    thumbnailImageUrl: '',
    hotspotImage: ''
  });

  const {
    register,
    getValues,
    handleSubmit,
    setValue,
    trigger,
    watch,
    formState: { errors }
  } = useForm({
    mode: 'onChange',
    reValidateMode: 'onChange'
  });
  const onSubmit = async (data) => {
    setLoading(true);
    const {
      title,
      body,
      media,
      video,
      visible,
      mediaContentThumbnail,
      coordinatesX,
      coordinatesY
    } = data;

    const formData = new FormData();
    formData.append('Id', id);
    formData.append('Title', title);
    formData.append('Body', body);

    formData.append('CoordinateX', coordinatesX);
    formData.append('CoordinateY', coordinatesY);
    formData.append('IsVisible', visible);
    if (listDelete.length > 0) {
      listDelete.forEach((item, index) => {
        formData.append(`ImageDeletes[${index}].Id`, item.id);
        formData.append(`ImageDeletes[${index}].FileName`, item.fileName);
      });
    }
    if (video !== undefined && video[0] !== undefined) {
      formData.append('HighLightMedia', video[0]);
    }

    if (media !== undefined && media[0] !== undefined) {
      formData.append('Media', media[0]);
    }
    if (
      mediaContentThumbnail !== undefined &&
      mediaContentThumbnail[0] !== undefined
    ) {
      formData.append('Thumbnail', mediaContentThumbnail[0]);
    }

    try {
      await hotspot.editHotspot(formData).then((res) => {
        setLoading(false);

        if (res.data.success) {
          onClose();
          handleOpenToast();
          changeToastMessage(res.data.message);
          setValue('video', undefined);
          setValue('media', undefined);
          setValue('mediaContentThumbnail', undefined);
          updateSuccess();
        } else {
          changeToastMessage(res.data.message);

          handleOpenToast();
        }
      });
    } catch (error) {
      // changeToastMessage(res.data.message);
      setLoading(false);
    }
  };

  useEffect(() => {
    if (open) {
      hotspot.getHotspotById(id).then((res) => {
        if (res.data.success) {
          const data = res.data.data;

          setValue('title', data.title);
          setValue('body', data.body ? data.body : '');
          setValue('coordinatesX', +data.coordinateX);
          setValue('coordinatesY', +data.coordinateY);

          handleChangeFormOpen(data.isVisible);
          setValue('visible', data.isVisible);

          setAllMedia({
            highlightMediaUrl: data.highlightMediaUrl
              ? data.highlightMediaUrl
              : '',
            mediaUrl: data.mediaUrl ? data.mediaUrl : '',
            thumbnailImageUrl: data.thumbnailUrl ? data.thumbnailUrl : '',
            hotspotImage: data.hostpotImageUrl ? data.hostpotImageUrl : ''
          });
        }
      });
    }
  }, [id, setValue, open]);

  const handleListFile = (file: File) => {
    if (file) {
      if (
        file.type.toLowerCase() === 'image/jpg' ||
        file.type.toLowerCase() === 'image/jpeg' ||
        file.type.toLowerCase() === 'image/png'
      ) {
        if (file.size <= 5012 * 1024)
          return <PreviewImage selectedFile={file} />;
        else return <p style={{ color: 'red' }}>This file is too large</p>;
      } else if (file.type.toLowerCase() === 'video/mp4') {
        if (file.size <= 2147483648)
          return <PreviewImage selectedFile={file} />;
        else return <p style={{ color: 'red' }}>This file is too large</p>;
      } else {
        return <p style={{ color: 'red' }}>Incorrect format</p>;
      }
    }
  };
  useEffect(() => {
    register('body', { required: true });
    register('visible');
    register('mediaContentThumbnail');
  }, []);

  const watchVideo = watch(['video', 'media']);
  const watchVideoThumbnail = watch(['mediaContentThumbnail']);
  const handleChange = (event) => {
    setChecked(event.target.checked);
    setValue('visible', event.target.checked);
  };
  const handleChangeFormOpen = (check: boolean) => {
    setChecked(check);
    setValue('visible', check);
  };

  const handleGetDataFromEditor = (data: string) => {
    setValue('body', data);
    trigger('body');
  };

  const removeFile = (name: string) => {
    setValue(name, undefined);
  };

  const removeLocalMedia = (name, slot) => {
    let fileName = name.slice(name.lastIndexOf('/') + 1);
    let arr = listDelete;
    arr.push({ id: id, fileName: fileName });
    setListDelete(arr);
    setAllMedia({ ...allMedia, [slot]: '' });
  };

  return (
    <Dialog maxWidth="md" fullWidth={true} onClose={onClose} open={open}>
      <DialogTitle>Edit</DialogTitle>
      <Dialog maxWidth="md" fullWidth={true} onClose={onClose} open={open}>
        <DialogTitle>
          {' '}
          <Typography
            variant="h3"
            sx={{ fontWeight: 'bold', textTransform: 'uppercase' }}
          >
            Edit Touchscreen Hotspot
          </Typography>
        </DialogTitle>
        <DialogContent sx={{ width: { md: '100%', xs: '100%', sm: '100%' } }}>
          <Box component="form" onSubmit={handleSubmit(onSubmit)}>
            <Box sx={{ mt: 3 }}>
              <TextField
                required
                defaultValue="Hello World"
                // required
                id="outlined-basic"
                label="Title (max. 80 characters)"
                variant="outlined"
                disabled={disable}
                error={errors.title}
                {...register('title', {
                  required: 'Title is require',
                  maxLength: {
                    value: 80,
                    message: 'Max length is 80 characters'
                  }
                })}
                sx={{ width: '100%' }}
              />
            </Box>
            {errors.title && (
              <Typography color="error">{errors.title.message}</Typography>
            )}
            <Box sx={{ mt: 3 }}>
              <Typography>Body text (max. 1000 characters)</Typography>
              <Jodit
                initialValue={getValues('body') as string}
                limit={1000}
                disable={disable}
                handleGetDataFromEditor={handleGetDataFromEditor}
              />
              {errors.body && (
                <Typography color="error">The body is required</Typography>
              )}
            </Box>
            <Box sx={{ mt: 3 }}>
              <Typography>Visible</Typography>
              <Switch
                color="secondary"
                checked={checked}
                disabled={disable}
                onChange={handleChange}
                inputProps={{ 'aria-label': 'controlled' }}
              />
            </Box>
            <Grid
              container
              rowSpacing={1}
              columnSpacing={{ xs: 1, sm: 1, md: 2 }}
              sx={{ mt: 3 }}
            >
              <Grid item xs={6}>
                <TextField
                  id="outlined-basic"
                  label="Coordinates X"
                  variant="outlined"
                  InputProps={{
                    inputProps: {
                      min: 0,
                      max: 1080,
                      step: '0.01'
                    }
                  }}
                  defaultValue="1"
                  type="number"
                  {...register('coordinatesX', {
                    min: 0,
                    max: 1080
                  })}
                  sx={{ width: '100%' }}
                  helperText={errors.coordinatesX && 'Warning: Max is 1080'}
                  disabled={disable}
                  error={errors.coordinatesX}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  id="outlined-basic"
                  label="Coordinates y"
                  variant="outlined"
                  InputProps={{
                    inputProps: {
                      min: 0,
                      max: 1920,
                      step: '0.01'
                    }
                  }}
                  error={errors.coordinatesY}
                  defaultValue="1"
                  helperText={errors.coordinatesY && 'Warning: Max is 1902'}
                  type="number"
                  {...register('coordinatesY', {
                    min: 0,
                    max: 1920
                  })}
                  sx={{ width: '100%' }}
                  disabled={disable}
                />
              </Grid>
            </Grid>

            <Box sx={{ mt: 3 }}>
              <Typography>
                Highlight image <span style={{ color: 'red' }}>*Optional*</span>{' '}
                (16:9, 1920 x 1080 px, JPG/PNG max 5MB)
              </Typography>
              <Button variant="contained" component="label" color="secondary">
                Upload File
                <input
                  type="file"
                  disabled={disable}
                  hidden
                  {...register('video')}
                  accept=".jpg, .png, .jpeg"
                />
              </Button>

              {watchVideo[0] && watchVideo[0][0] && (
                <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
                  <Typography sx={{ fontStyle: 'italic' }}>
                    {handleListFile(watchVideo[0][0])}
                  </Typography>
                  <CloseIcon
                    color="error"
                    style={{ cursor: 'pointer' }}
                    onClick={() => {
                      removeFile('video');
                    }}
                  />
                </Box>
              )}

              {(!watchVideo[0] || !watchVideo[0][0]) &&
                allMedia.highlightMediaUrl && (
                  <Box
                    sx={{ display: 'flex', justifyContent: 'space-between' }}
                  >
                    <Typography sx={{ mt: 3 }}>
                      <img
                        src={allMedia.highlightMediaUrl}
                        alt="led"
                        width="200"
                        height="auto"
                      />
                    </Typography>
                    <CloseIcon
                      color="error"
                      style={{ cursor: 'pointer' }}
                      onClick={() => {
                        removeLocalMedia(
                          allMedia.highlightMediaUrl,
                          'highlightMediaUrl'
                        );
                      }}
                    />
                  </Box>
                )}
            </Box>
            {/* <Box sx={{ mt: 3 }}>
              <Typography>
                Pop-up Image <span style={{ color: 'red' }}>*Optional*</span>{' '}
                (16:9, 1920 x 1080 px, JPG/PNG max. 5MB or MP4 max. 2GB)
              </Typography>
              <Button variant="contained" component="label" color="secondary">
                Upload File
                <input
                  type="file"
                  disabled={disable}
                  hidden
                  {...register('media')}
                  accept=".jpg, .png, .jpeg, .mp4"
                />
              </Button>
              {watchVideo[1] && watchVideo[1][0] && (
                <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
                  <Typography sx={{ fontStyle: 'italic' }}>
                    {handleListFile(watchVideo[1][0])}
                  </Typography>
                  <CloseIcon
                    color="error"
                    style={{ cursor: 'pointer' }}
                    onClick={() => {
                      removeFile('media');
                    }}
                  />
                </Box>
              )}
              {(!watchVideo[1] || !watchVideo[1][0]) && allMedia.mediaUrl && (
                <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
                  <Typography sx={{ mt: 3 }}>
                    {allMedia.mediaUrl.includes('mp4') ? (
                      <video
                        src={allMedia.mediaUrl}
                        width="200"
                        height="125"
                        controls
                      ></video>
                    ) : (
                      <img
                        src={allMedia.mediaUrl}
                        alt="led"
                        width="200"
                        height="125"
                      />
                    )}
                  </Typography>
                  <CloseIcon
                    color="error"
                    style={{ cursor: 'pointer' }}
                    onClick={() => {
                      removeLocalMedia(allMedia.mediaUrl, 'mediaUrl');
                    }}
                  />
                </Box>
              )}
            </Box> */}
            {((watchVideo[1] &&
              watchVideo[1][0] &&
              watchVideo[1][0].type === 'video/mp4') ||
              (allMedia.thumbnailImageUrl &&
                allMedia.mediaUrl.includes('mp4'))) && (
              <Box sx={{ mt: 3 }}>
                <Typography>
                  Thumbnail image (16:9, 1920 x 1080 px, JPG/PNG max 5MB)
                </Typography>
                <Button variant="contained" component="label" color="secondary">
                  Upload File
                  <input
                    disabled={disable}
                    type="file"
                    hidden
                    {...register(`mediaContentThumbnail`)}
                    accept=".jpg,.png, jpeg"
                  />
                </Button>

                {watchVideoThumbnail[0] && watchVideoThumbnail[0][0] && (
                  <Box
                    sx={{ display: 'flex', justifyContent: 'space-between' }}
                  >
                    <Typography sx={{ fontStyle: 'italic' }}>
                      {handleListFile(watchVideoThumbnail[0][0])}
                    </Typography>
                    <CloseIcon
                      color="error"
                      style={{ cursor: 'pointer' }}
                      onClick={() => {
                        removeFile('mediaContentThumbnail');
                      }}
                    />
                  </Box>
                )}

                {(!watchVideoThumbnail[0] || !watchVideoThumbnail[0][0]) &&
                  allMedia.thumbnailImageUrl && (
                    <Box
                      sx={{ display: 'flex', justifyContent: 'space-between' }}
                    >
                      <Typography sx={{ mt: 3 }}>
                        <img
                          src={allMedia.thumbnailImageUrl}
                          alt="led"
                          width="200"
                          height="125"
                        />
                      </Typography>
                      <CloseIcon
                        style={{ cursor: 'pointer' }}
                        color="error"
                        onClick={() => {
                          removeLocalMedia(
                            allMedia.thumbnailImageUrl,
                            'thumbnailImageUrl'
                          );
                        }}
                      />
                    </Box>
                  )}
              </Box>
            )}
            <Box sx={{ mt: 3 }}>
              <Button
                color="secondary"
                type="submit"
                disabled={disable}
                variant="contained"
                sx={{ width: '100%' }}
              >
                {loading ? <CircularProgress color="info" /> : 'Submit'}
              </Button>
            </Box>
          </Box>
        </DialogContent>
      </Dialog>
    </Dialog>
  );
}

SimpleDialog.propTypes = {
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
  id: PropTypes.string.isRequired,
  disable: PropTypes.bool.isRequired,
  handleOpenToast: PropTypes.func.isRequired,
  changeToastMessage: PropTypes.func.isRequired
};
interface PropsId {
  id: string;
  disable: boolean;
}
function Edit({ id, disable }: PropsId) {
  const [open, setOpen] = useState(false);
  const [openToast, setOpenToast] = useState(false);
  const [msg, setMsg] = useState<string>('');
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleCloseToast = () => {
    setOpenToast(false);
  };

  const handleOpenToast = () => {
    setOpenToast(true);
  };

  const changeToastMessage = (msg: string) => {
    setMsg(msg);
  };
  return (
    <>
      {disable ? (
        <RemoveRedEyeIcon fontSize="small" onClick={handleClickOpen} />
      ) : (
        <EditTwoToneIcon fontSize="small" onClick={handleClickOpen} />
      )}

      <SimpleDialog
        open={open}
        onClose={handleClose}
        id={id}
        changeToastMessage={changeToastMessage}
        handleOpenToast={handleOpenToast}
        disable={disable}
      />
      <Snackbar
        autoHideDuration={3000}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={openToast}
        onClose={handleCloseToast}
        message={msg}
        key={'top right'}
      />
    </>
  );
}

export default Edit;
