import { Card, LinearProgress } from '@mui/material';
import { useContext, useEffect, useState } from 'react';
import hotspot from 'src/api/hotspot';
import { AuthContext } from 'src/App';
import { Hotspot } from 'src/models';
import RecentOrdersTable from './RecentOrdersTable';
function RecentOrders() {
  const [loading, setLoading] = useState<boolean>(false);
  const [cryptoOrders, setCryptoOrders] = useState<Hotspot[]>([]);
  const [idStation, setIdStation] = useState<string>('');
  const [backgroundHostpot, setBackgroundHostpot] = useState<string>('');

  const { updated } = useContext(AuthContext);
  useEffect(() => {
    setLoading(true);
    hotspot.getHotspot().then((res) => {
      if (res.data.success) {
        setLoading(false);
        setCryptoOrders(res.data.data);
        setIdStation(res.data.data[0].stationId);
        hotspot
          .getBackgroundHotspot(res.data.data[0].stationId)
          .then((response) => {
            const { data, success } = response.data;
            if (success) {
              setBackgroundHostpot(data.backgroundHostpotUrl);
            }
          });
      }
    });
  }, [updated]);

  return (
    <Card>
      <RecentOrdersTable
        cryptoOrders={cryptoOrders}
        idStation={idStation}
        backgroundHostpot={backgroundHostpot}
      />
      {loading && <LinearProgress />}
    </Card>
  );
}

export default RecentOrders;
