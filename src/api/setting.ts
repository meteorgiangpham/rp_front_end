import axiosUserClient from './axiosUserClient';
const settingApi = {
  getSetting(): Promise<any> {
    return axiosUserClient.get('Setting/get-setting');
  }
};
export default settingApi;
