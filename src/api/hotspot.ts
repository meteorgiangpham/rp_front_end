import axiosUserClient from './axiosUserClient';
const hostpot = {
  getHotspot(): Promise<any> {
    const url = '/Admins/get-station-host-pot';
    return axiosUserClient.get(url);
  },

  getHotspotById(id: number): Promise<any> {
    const url = `/Admins/get-station-hostpot-by-id?id=${id}`;
    return axiosUserClient.get(url);
  },
  editHotspot(body: any): Promise<any> {
    return axiosUserClient.put('/Admins/update-station-hostpot', body);
  },
  updateBackgroundHotspot(body: any): Promise<any> {
    return axiosUserClient.put('/Admins/update-background-hostpot', body);
  },
  getBackgroundHotspot(id: number): Promise<any> {
    const url = `/Admins/get-background-host-pot?stationId=${id}`;
    return axiosUserClient.get(url);
  }
};
export default hostpot;
