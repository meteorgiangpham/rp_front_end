import { ReactNode } from 'react';

import History from '@mui/icons-material/History';
import TableChartTwoToneIcon from '@mui/icons-material/TableChartTwoTone';
import AllOutIcon from '@mui/icons-material/AllOut';
// import AssessmentIcon from '@mui/icons-material/Assessment';
import PreviewIcon from '@mui/icons-material/Preview';
import AutoStoriesIcon from '@mui/icons-material/AutoStories';
import ContactsIcon from '@mui/icons-material/Contacts';
import ControlCameraSharpIcon from '@mui/icons-material/ControlCameraSharp';
export interface MenuItem {
  link?: string;
  icon?: ReactNode;
  badge?: string;
  items?: MenuItem[];
  name: string;
}

export interface MenuItems {
  items: MenuItem[];
  heading: string;
}

const menuItems: MenuItems[] = [
  {
    heading: 'Dashboards',
    items: [
      {
        name: 'SHOW EDIT HISTORY',
        icon: History,
        link: `${process.env.REACT_APP_BASE_NAME}/system/audit`
      }
    ]
  },
  {
    heading: 'FAÇADE A',
    items: [
      {
        name: 'TOUCHSCREEN SCREENSAVER',
        icon: TableChartTwoToneIcon,
        link: `${process.env.REACT_APP_BASE_NAME}/facade/screensaver`
      },
      {
        name: 'TOUCHSCREEN HOTSPOT',
        icon: AllOutIcon,
        link: `${process.env.REACT_APP_BASE_NAME}/facade/hotspot`
      },
      {
        name: 'LED PANEL',
        icon: ControlCameraSharpIcon,
        link: `${process.env.REACT_APP_BASE_NAME}/facade/led`
      },
      {
        name: 'PREVIEW HOTSPOT',
        icon: PreviewIcon,
        link: `${process.env.REACT_APP_BASE_NAME}/facade/preview`
      }
    ]
  },
  {
    heading: 'Corridor B',
    items: [
      {
        name: 'SCREENSAVER',
        icon: TableChartTwoToneIcon,
        link: `${process.env.REACT_APP_BASE_NAME}/corridor/screensaver`
      },
      {
        name: 'PAGES',
        icon: AutoStoriesIcon,
        link: `${process.env.REACT_APP_BASE_NAME}/corridor/pages`
      }
    ]
  },
  // {
  //   heading: 'System',
  //   items: [
  //     {
  //       name: 'USER MANAGEMENT',
  //       link: `${process.env.REACT_APP_BASE_NAME}/system/user`,
  //       icon: ContactsIcon
  //     }
  //   ]
  // }
];

export default menuItems;
