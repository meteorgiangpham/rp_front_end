import { useContext, useMemo, useRef, useState } from 'react';

import {
  Box,
  Button,
  Hidden,
  lighten,
  Popover,
  Typography
} from '@mui/material';

import { styled } from '@mui/material/styles';
import ExpandMoreTwoToneIcon from '@mui/icons-material/ExpandMoreTwoTone';
import LockOpenTwoToneIcon from '@mui/icons-material/LockOpenTwoTone';
import { useNavigate } from 'react-router';
import { AuthContext } from 'src/App';
import jwt_decode from 'jwt-decode';
import userApi from 'src/api/userApi';
const UserBoxButton = styled(Button)(
  ({ theme }) => `
        padding-left: ${theme.spacing(1)};
        padding-right: ${theme.spacing(1)};
`
);

const MenuUserBox = styled(Box)(
  ({ theme }) => `
        background: ${theme.colors.alpha.black[5]};
        padding: ${theme.spacing(2)};
`
);

const UserBoxText = styled(Box)(
  ({ theme }) => `
        text-align: left;
        padding-left: ${theme.spacing(1)};
`
);

const UserBoxLabel = styled(Typography)(
  ({ theme }) => `
        font-weight: ${theme.typography.fontWeightBold};
        color: ${theme.palette.primary.main};
        display: block;
`
);

const UserBoxDescription = styled(Typography)(
  ({ theme }) => `
        color: ${lighten(theme.palette.primary.main, 0.5)}
`
);

function HeaderUserbox() {
  const { handleLoginOut } = useContext(AuthContext);

  const ref = useRef<any>(null);
  const [isOpen, setOpen] = useState<boolean>(false);
  const nav = useNavigate();
  const handleOpen = (): void => {
    setOpen(true);
  };

  const handleClose = (): void => {
    setOpen(false);
  };

  const handleSignOut = () => {
    userApi.logOut().then((res) => {
      if (res.data.success) {
        localStorage.removeItem('access_token');
        handleLoginOut();
        if (!Boolean(localStorage.removeItem('access_token'))) {
          nav(`${process.env.REACT_APP_BASE_NAME}`);
        }
      }
    });
  };
  const token = localStorage.getItem('access_token');
  const getUserNameFromToken = useMemo(() => {
    var decoded = jwt_decode(token) as any;
    if (decoded) return decoded.name;
  }, [token]);

  return (
    <>
      <UserBoxButton color="secondary" ref={ref} onClick={handleOpen}>
        <Hidden mdDown>
          <UserBoxText>
            <UserBoxLabel variant="body2"> Hello</UserBoxLabel>
            <UserBoxDescription variant="body1">
              {getUserNameFromToken}
            </UserBoxDescription>
          </UserBoxText>
        </Hidden>
        <Hidden smDown>
          <ExpandMoreTwoToneIcon sx={{ ml: 1 }} color="primary" />
        </Hidden>
      </UserBoxButton>
      <Popover
        anchorEl={ref.current}
        onClose={handleClose}
        open={isOpen}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'right'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right'
        }}
      >
        <MenuUserBox sx={{ minWidth: 210 }} display="flex">
          <UserBoxText>
            <UserBoxLabel variant="body2">
              <span
                style={{
                  color: 'rgb(182, 186, 207)',
                  fontWeight: 500,
                  fontSize: '12px'
                }}
              >
                User:{' '}
              </span>
              {getUserNameFromToken}
            </UserBoxLabel>
          </UserBoxText>
        </MenuUserBox>

        <Box sx={{ m: 1 }}>
          <Button color="primary" fullWidth onClick={handleSignOut}>
            <LockOpenTwoToneIcon sx={{ mr: 1 }} />
            Sign out
          </Button>
        </Box>
      </Popover>
    </>
  );
}

export default HeaderUserbox;
